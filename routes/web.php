<?php

use App\Http\Controllers\AbsenteeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\UserChannelController;
use App\Http\Controllers\UserController;
use App\Models\Absentee;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("auth.login");
});

Route::prefix("auth")->group(function() {
    Route::controller(AuthController::class)->group(function() {
        Route::get("login", "loginIndex")->name("auth.login");
        Route::post("login", "loginPost")->name("auth.login.post");

        Route::get("register", "registerIndex")->name("auth.register");
        Route::post("register", "register")->name("auth.register.post");

        Route::get("email/verify/{encrypted_code}","verifyEmail")->name("auth.verify-email");
        Route::post("verify-password", "verifyPassword")->name("auth.verify-password");

        Route::get("forgot-password", "forgotPasswordIndex")->name("forgot.password.index");
        Route::post("forgot-password/generate-link", "generateLinkForgotPassword")->name("forgot.password.post");
        Route::get("forgot-password/{code_link}", "linkResetPassowrd")->name("forgot.password.link");
        Route::post("forgot-password/{code_link}", "postResetPassowrd")->name("forgot.password.change");
    });
});

Route::middleware("auth")->group(function() {
    Route::post('/store-token', [NotificationController::class, 'updateDeviceToken'])->name('store.token');
    Route::prefix("user")->group(function() {
        Route::controller(UserController::class)->group(function() {
            Route::get("qrcode-scanner","scannerQrCode")->name("user.scanner");
            Route::get("dashboard", "dashboardIndex")->name("user.dashboard");

            Route::prefix("my-profile")->group(function() {
                Route::get("/","myProfileIndex")->name("user.my-profile");
                Route::post("/","myProfile")->name("user.my-profile.post");
            });

            Route::post("logout","logout")->name("user.logout");
        });
    });

    Route::prefix("channel")->group(function() {
        Route::controller(ChannelController::class)->group(function() {
            Route::get("inviation/{random_code}","linkInvitationChannel")->name("channel.url.invitation");
            Route::post("create","createChannel")->name("channel.create");
            Route::post("join","joinChannel")->name("channel.join");
            Route::get("all","getAllChannelUser")->name("channel.get.all");
        });

        Route::middleware("auth.channel")->group(function() {
            Route::prefix("{code_channel}")->group(function() {
                Route::controller(ChannelController::class)->group(function() {
                    Route::get("/","dashboardChannel")->name("channel.dashboard");
                    Route::post("leave-channel","leaveChannel")->name("channel.leave");
                });

                Route::controller(UserChannelController::class)->group(function() {
                    Route::prefix("control-user")->group(function() {
                        Route::get("/","listUserIndex")->name("channel.user.list.index");
                        Route::get("list-user","getListUser")->name("channel.user.list.all");
                        Route::get("searching-user/{username}","searchingUser")->name("channel.user.searching");
                        Route::get("user-logs","userLogIndex")->name("channel.user.log.index");
                        Route::get("list-user-logs","listUserLogs")->name("channel.user.logs");
    
                        Route::prefix("admin")->group(function() {
                            Route::post("change-status","changeStatus")->name("channel.change-status.user");
                            Route::post("kick","kickUser")->name("channel.kick.user");
                            Route::get("invite-user","inviteUserIndex")->name("channel.invite.index");
                            Route::post("invite-user","inviteUser")->name("channel.invite.user");
                            Route::get("list-invitations","listInvitations")->name("channel.invitations");
                            Route::post("cancel-invitation","cancelInviation")->name("channel.invitations.cancel");
                        });
                    });
                });

                Route::controller(AbsenteeController::class)->group(function() {
                    Route::prefix("absentee")->group(function() {
                        Route::get("/","absenteeIndex")->name("channel.absentee.index");
                        Route::post("/","createAbsentee")->name("channel.absentee.open");
                        Route::put("/","closeAbsentee")->name("channel.absentee.close");
                        Route::get("/list","absenteeList")->name("channel.absentee.list");

                        Route::prefix("event")->group(function() {
                            Route::get("view/{code_absen}","userAbsenteeEventIndex")->name("channel.absentee.users.index");
                            Route::get("{code_absen}","userAbsenteeEvent")->name("channel.absentee.users");
                            Route::delete("delete-absent/{id}","deleteAbsentByUserChannelId")->name("channel.absentee.delete-absent");
                            Route::post("manual-absent/{absent_id}","manualAbsent")->name("channel.absentee.manual-absent");
                            Route::post("leave/sent/{code_absen}","userAbsenteeLeaveEvent")->name("channel.absentee.leave");
                            Route::get("leave/{code_absen}","userLeaveAbsenteeEventView")->name("channel.absentee.users-leave");
                            Route::get("list/{id_absent}","userAbsenteeEventList")->name("channel.absentee.users.list");
                            Route::get("print/{code_absen}","userAbsenteeEventIndexPrint")->name("channel.absentee.users.print");
                            Route::get("print/qrcode/{code_absen}","userAbsenteeEventIndexPrintQrcode")->name("channel.absentee.users.print.qrcode");
                        });
                    });

                    Route::prefix("my-absentee")->group(function() {
                        Route::get("/","myAbsenteeIndex")->name("channel.my-absentee.index");
                        Route::get("/list","myAbsenteeList")->name("channel.my-absentee.list");
                    });
                });

                Route::controller(EventController::class)->group(function() {
                    Route::prefix("event")->group(function() {
                        Route::get("list", "listEventIndex")->name("channel.event.index");
                        Route::get("/", "listEvent")->name("channel.event.list");
                        Route::post("/","createEvent")->name("channel.event.create");
                        Route::delete("/","deleteEvent")->name("channel.event.delete");
                        Route::put("/","editEvent")->name("channel.event.update");
                    });
                });
            });
        });


    });
});
