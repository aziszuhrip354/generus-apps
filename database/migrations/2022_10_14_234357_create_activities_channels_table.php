<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities_channels', function (Blueprint $table) {
            $table->id();
            $table->string("code_channel")->index();
            $table->foreign("code_channel")->references("code_channel")->on("channels");
            $table->string("name_activity", 100)->index();
            $table->string("start_at", 15);
            $table->string("end_at", 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities_channels');
    }
};
