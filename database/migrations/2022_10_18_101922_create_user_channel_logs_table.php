<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_channel_logs', function (Blueprint $table) {
            $table->id();
            $table->string("updated_by", 50);
            $table->string("code_channel")->index();
            $table->foreign("code_channel")->references("code_channel")->on("channels");
            $table->enum("type_log",["join","create","kick","leave","update-status","cancel"]);
            $table->text("description");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_channel_logs');
    }
};
