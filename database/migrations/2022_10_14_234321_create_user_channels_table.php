<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_channels', function (Blueprint $table) {
            $table->id();
            $table->string("code_channel")->index();
            $table->foreign("code_channel")->references("code_channel")->on("channels");
            $table->unsignedBigInteger("user_id")->index();
            $table->foreign("user_id")->references("id")->on("users");
            $table->tinyInteger("type_user")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_channels');
    }
};
