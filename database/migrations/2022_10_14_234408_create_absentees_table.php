<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absentees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("activity_channel_id")->index();
            $table->foreign("activity_channel_id")->references("id")->on("activities_channels");
            $table->string("code_channel")->index();
            $table->foreign("code_channel")->references("code_channel")->on("channels");
            $table->unsignedBigInteger("user_channel_id")->index();
            $table->foreign("user_channel_id")->references("id")->on("user_channels");
            $table->integer("total_absen")->default(0);
            $table->string("code_absen", 30)->unique();
            $table->tinyInteger("late_minute_tolerance")->default(0);
            $table->tinyInteger("status")->default(1)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absentees');
    }
};
