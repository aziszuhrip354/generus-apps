<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_invitations', function (Blueprint $table) {
            $table->id();
            $table->string("invited_by", 50);
            $table->unsignedBigInteger("user_id")->index();
            $table->foreign("user_id")->references("id")->on("users");
            $table->string("code_channel")->index();
            $table->foreign("code_channel")->references("code_channel")->on("channels");
            $table->tinyInteger("status")->default(0);
            $table->enum("type_invitation", ["wa","email"]);
            $table->string("random_code", 50)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_invitations');
    }
};
