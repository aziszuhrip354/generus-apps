<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absentee_user_channels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_channel_id")->index();
            $table->foreign("user_channel_id")->references("id")->on("user_channels");
            $table->unsignedBigInteger("absentee_id")->index();
            $table->foreign("absentee_id")->references("id")->on("absentees");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absentee_user_channels');
    }
};
