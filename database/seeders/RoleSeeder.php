<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listRoles = ["muda-mudi", "pra-remaja/remaja", "lansia", "umum"];
        foreach ($listRoles as $roles) { 
            try {
                $newRole = new Role();
                $newRole->name = $roles;
                $newRole->save();
            } catch (\Throwable $th) {
                echo "\n{$th->getMessage()}";
            }
        }
        
    }
}
