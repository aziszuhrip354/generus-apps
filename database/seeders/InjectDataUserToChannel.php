<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserChannel;
use Carbon\Carbon;

class InjectDataUserToChannel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $codeChannel = "hbZIOE";
        $mudaMudi = [
            [
                "nama" => "Suci rohalia putri",
                "jenis_kelamin" => "wanita",
                "email" => "sucirohaliaputri@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Fadila sita",
                "jenis_kelamin" => "wanita",
                "email" => "fadilasita@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Dyah tri puspitasari",
                "jenis_kelamin" => "wanita",
                "email" => "dyahtripuspitasari@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Regitha andra gadis",
                "jenis_kelamin" => "wanita",
                "email" => "regithaandragadis@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Resspati kalih fatah",
                "jenis_kelamin" => "pria",
                "email" => "resspatikalihfatah@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Auretha reyza somya",
                "jenis_kelamin" => "wanita",
                "email" => "aurethareyzasomya@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Suci amalia",
                "jenis_kelamin" => "wanita",
                "email" => "suciamalia@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Ken daewood salam",
                "jenis_kelamin" => "pria",
                "email" => "kendaewoodsalam@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Novian bagus",
                "jenis_kelamin" => "pria",
                "email" => "novianbagus@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Nisa intan kamila",
                "jenis_kelamin" => "wanita",
                "email" => "nisaintankamila@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Rangga",
                "jenis_kelamin" => "pria",
                "email" => "rangga.kps@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Fattan",
                "jenis_kelamin" => "pria",
                "email" => "fattan@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "James",
                "jenis_kelamin" => "pria",
                "email" => "james@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Akmal",
                "jenis_kelamin" => "pria",
                "email" => "akmal@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Dika",
                "jenis_kelamin" => "pria",
                "email" => "dika@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "Fian",
                "jenis_kelamin" => "pria",
                "email" => "fian@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
            [
                "nama" => "ferdianto",
                "jenis_kelamin" => "pria",
                "email" => "ferdianto.praja@gmail.com",
                "role" => "muda-mudi",
                "birthday" => \Faker\Factory::create()->dateTimeBetween("-30 years", "-18 years")->format("Y-m-d")
            ],
        ];
        foreach ($mudaMudi as $m) {
            $createNewUser = new User();
            $createNewUser->name = $m['nama'];
            $createNewUser->email = $m['email'];
            $createNewUser->no_hp =  faker()->randomElement(['+628', '08']) . faker()->numerify('1##-####-####');
            $createNewUser->password = bcrypt("adminadmin");
            $createNewUser->uuid = faker()->uuid;
            $createNewUser->gender = $m['jenis_kelamin'];
            $createNewUser->role = $m['role'];
            $createNewUser->birthday = $m['birthday'];
            $createNewUser->email_verified_at = Carbon::now();
            $createNewUser->save();

            $addToGroup = new UserChannel();
            $addToGroup->code_channel = $codeChannel;
            $addToGroup->user_id = $createNewUser->id;
            $addToGroup->type_user = 0;
            $addToGroup->save();
        }
    }
}
