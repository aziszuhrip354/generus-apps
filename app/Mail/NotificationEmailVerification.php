<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationEmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $dataArr = [];
        $name = $data['name'];
        $gender = $data['gender'];
        $email = $data['email'];
        $linkVerification = $data['link_verification'];
        $dataArr[] = "name";
        $dataArr[] = "linkVerification";
        $dataArr[] = "gender";
        $dataArr[] = "email";
        return $this->subject("KONFIRMASI REGISTRASI PENDAFTARAN GENERUS APPS")
                    ->from("aziszuhrip354@gmail.com", "DEVELOPER GENERUS APPS")
                    ->view('email.email-verification', compact($dataArr));
                    
        // return $this->view('view.name');
    }
}
