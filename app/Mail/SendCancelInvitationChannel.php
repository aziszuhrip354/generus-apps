<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCancelInvitationChannel extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $dataArr = [];
        $name = $data['name'];
        $channelName = $data['channel_name'];
        $canceledBy = $data['canceled_by'];
        $gender = $data['gender'];
        $dataArr[] = "name";
        $dataArr[] = "canceledBy";
        $dataArr[] = "channelName";
        $dataArr[] = "gender";
        return $this->subject("GENERUS APPS - PEMBATALAN UNDANGAN MASUK CHANNEL {$channelName}!")
                    ->from("aziszuhrip354@gmail.com", "DEVELOPER GENERUS APPS")
                    ->view('email.email-cancel-invitation-channel', compact($dataArr));
                    
        // return $this->view('view.name');
    }
}
