<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailNotificationService extends Mailable
{
    use Queueable, SerializesModels;

    public $contentSubject;
    public $viewName;
    public array $datas;
    public $pathAttachment;

    /**
     * Create a new message instance.
     *
     * @param string $contentSubject
     * @param string $viewName
     * @param array $datas
     * @param string|null $pathAttachment
     */
    public function __construct(
        $contentSubject,
        $viewName, 
        array $datas, 
        $pathAttachment = null
    ) {
        $this->contentSubject = $contentSubject;
        $this->viewName = $viewName;
        $this->datas = $datas;
        $this->pathAttachment = $pathAttachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->subject($this->contentSubject)
                     ->view($this->viewName)
                     ->with($this->datas);

        // Add attachment if provided
        if ($this->pathAttachment) {
            $mail->attach($this->pathAttachment);
        }

        return $mail;
    }
}
