<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absentee extends Model
{
    use HasFactory;

    public function activity() {
        return $this->belongsTo(ActivitiesChannel::class, "activity_channel_id", "id");
    }
}
