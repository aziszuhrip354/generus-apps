<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsenteeUserChannel extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_channel_id',
        'absentee_id',
        'reason',
        'created_at',
        'updated_at',
        'leave_category'
    ];
}
