<?php

use App\Models\UserChannel;
use Faker\Factory as Faker;
    use Illuminate\Support\Facades\Auth;
    use App\Models\UserChannelLogs;

    function test() {
        dd("Test");
    }

    function printAllError($arr) {
        try {
            $str = "<ul class='alert text-mute px-4 toastr' role='alert'>";
            if(count($arr) > 0) {
               foreach($arr as $errors) {
                    $str .= "<li><h6 class='alert alert-danger font-italic'>{$errors}</h6></li>";
               }
               $str .= "</ul>";
               return $str;
            }
            return "";
        } catch (\Throwable $th) {
            return "";
        }
    }

    function faker() {
        return Faker::create("id_ID");
    }

    function showAlert($message, $color = "danger") {
        
        if(!$message) {
            return "";
        }
        switch ($color) {
            case 'danger':
                $classAlert = "alert-danger";
                break;
            case 'primary':
                $classAlert = "alert-primary";
                break;
            case 'success':
                $classAlert = "alert-success";
                break;
            default:
                $classAlert = "alert-danger";
                break;
        }
        return "<div class='alert {$classAlert} text-small fw-bold toastr'>
                    <span>{$message}</span>
                </div>";
    }

    function convertNoHpTo62($noHp) {
        $nomorHp =  str_replace(" ","",str_replace("-","", $noHp));
        if($nomorHp[0] == "0") {
            $nomorHp = substr_replace($nomorHp,"",0,1);
            $nomorHp = "62".$nomorHp;
            return $nomorHp;
        } else if($nomorHp[0].$nomorHp[1] == "62"){
            return $nomorHp;
        }

        return false;
    }

    function user() {
        return Auth::user();
    }

    function userLogs($updatedBy, $typeLog, $description, $code_channel) {
        try {
            $createUserLogs = new UserChannelLogs();
            $createUserLogs->updated_by = $updatedBy;
            $createUserLogs->type_log = $typeLog;
            $createUserLogs->description = $description;
            $createUserLogs->code_channel = $code_channel;
            $createUserLogs->save();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
        return false;
    }

    function deleteKeyArr($arr, $target) {
        if (($key = array_search($target, $arr)) !== false) {
            unset($arr[$key]);
            return $arr;
        }
        return false;
    }

    function sessionToArray($session){
        $codeChannelsUser = json_decode(json_encode($session), true);
        return $codeChannelsUser;
    }


    function collectionToArr($collections) {
        $arr = [];
        foreach($collections as $c) {
            $arr[] = $c;
        }

        return $arr;
    }

    function userChannel($code_channel, $userId) {
        return UserChannel::where("code_channel", $code_channel)
                            ->where("user_id", $userId)
                            ->first();
    }
?>