<?php

namespace App\Http\Controllers;

use App\Models\College;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function dashboardIndex()
    {
        return view("app.dashboard.index");
    }

    public function scannerQrCode()
    {
        return view("app.dashboard.qrcode-scanner");
    }

    public function myProfileIndex()
    {
        $getCollege = College::where("user_id", user()->id)->first();
        $wa = substr_replace(user()->no_hp,"",0,1);
        $dataArr = [];
        $dataArr[] = "getCollege";
        $dataArr[] = "wa";
        return view("app.dashboard.my-profile", compact($dataArr));
    }

    public function myProfile(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => false,
            "data" => $request->all(),
        ];

        $rules = [
            "email" => ["required", 'email:dns'],
            "name" => ["required"],
            "no_hp" => ["required","min:8"],
            "gender" => ["required"],
        ];

        $message = [
            "email.required" => "Email tidak boleh kosong!",
            "email.email" => "Email Anda tidak valid!",
            "email.unique" => "Email Anda telah digunakan!",
            "no_hp.required" => "Nomor HP tidak boleh kosong!",
            "no_hp.min" => "Nomor HP terlalu pendek!",
            "name.required" => "Nama tidak boleh kosong!",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            if(user()->email != $request->email) {
                if(User::where("email", $request->email)->first()) {
                    $response["code"] = 400;
                    $response["status"] = false;
                    $response["message"] = "Email telah digunakan";
                    return response()->json($response, $response["code"]);
                }
            }
            $user = user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->asal_daerah = $request->asal_daerah;
            $user->alamat = $request->address;
            $user->no_hp = $request->no_hp;
            $user->update();
            
            if($request->kampus == null || $request->jurusan == null) {
                if($request->kampus == null) {
                    $response["code"] = 400;
                    $response["status"] = false;
                    $response["message"] = "Silahkan isi nama kampus Anda!";
                    return response()->json($response, $response["code"]);
                } else if($request->prodi == null){
                    $response["code"] = 400;
                    $response["status"] = false;
                    $response["message"] = "Silahkan isi nama jurusan kampus Anda!";
                    return response()->json($response, $response["code"]);
                }
                $findKampus = College::where("user_id",$user->id)->first();
                if(!$findKampus) {
                    $createNewKampus = new College();
                    $createNewKampus->user_id = $user->id;
                    $createNewKampus->nama_kampus = $request->kampus;
                    $createNewKampus->jurusan = $request->prodi;
                    $createNewKampus->save();
                } else {
                    $findKampus->nama_kampus = $request->kampus;
                    $findKampus->jurusan = $request->prodi;
                    $findKampus->update();
                }
            }
            
            $response["message"] = "Berhasil menyimpan data";
        }

        return response()->json($response, $response["code"]);
    }

    public function logout(Request $request)
    {
        user()->remember_token = null;
        user()->save();
        $request->session()->inValidate();
        $request->session()->regenerateToken();

        return redirect()->route("auth.login");
    }

}
