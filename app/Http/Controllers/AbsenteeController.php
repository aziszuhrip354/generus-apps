<?php

namespace App\Http\Controllers;

use App\Models\Absentee;
use App\Models\AbsenteeUserChannel;
use App\Models\ActivitiesChannel;
use App\Models\Role;
use App\Models\User;
use App\Models\UserChannel;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbsenteeController extends Controller
{
    public function sendNotification($title, $body, $code_channel)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $FcmToken = User::join("user_channels", "user_channels.user_id", "=", "users.id")
            ->where("user_channels.code_channel", $code_channel)
            ->where("users.id", "!=", user()->id)
            ->whereNotNull('users.device_token')
            ->pluck('users.device_token');
        $serverKey = 'AAAAeGwgsRA:APA91bGStBAKWQkl1_8XLfZdg_QJvrENYuEEm0UCQC1fZoDJqyKLzDsd0lAH8SyxU2rz5_5eoEzO_xufM7FlOdfaIQxUXTVFXBYAUalHIDcBVod_p_MPflOe9UzHv-OomweUiHN50HA3'; // ADD SERVER KEY HERE PROVIDED BY FCM
        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => $title,
                "body" => $body,
            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        // FCM response
        return $result;
    }

    public function absenteeIndex($code_channel)
    {
        $absenteeId = Absentee::where("code_channel", $code_channel)
            ->where("created_at", ">=", Carbon::now()->format("Y-m-d") . " 00:00:00")
            ->pluck("activity_channel_id");
        if (count($absenteeId)) {
            $absenteeValid = ActivitiesChannel::where("code_channel", $code_channel)->whereNotIn("id", $absenteeId)->orderBy("id", "ASC")->get();
        } else {
            $absenteeValid = ActivitiesChannel::where("code_channel", $code_channel)->orderBy("id", "ASC")->get();
        }
        $listRole = Role::pluck("name");
        $dataArr[] = "listRole";
        $dataArr[] = "absenteeValid";
        $dataArr[] = "code_channel";
        return view("app.dashboard-channel.create-absentee", compact($dataArr));
    }

    public function myAbsenteeIndex($code_channel)
    {
        $dataArr[] = "code_channel";
        return view("app.dashboard-channel.my-absentee", compact($dataArr));
    }

    public function createAbsentee(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $rules = [
            "absentee_id" => ["required"],
            "assign_role" => ["required", "array"],
            'assign_role.*' => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules, ["assign_role.required" => "Silahkan pilih role dahulu.."]);
        if ($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            $id = base64_decode($request->absentee_id);
            $event = ActivitiesChannel::find($id);
            if (!$event) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Event not found!";
            } else {
                $assignTo = implode(",", $request->assign_role);
                $createNewAbsentee = new Absentee();
                $createNewAbsentee->code_channel = $code_channel;
                $createNewAbsentee->activity_channel_id = $id;
                $createNewAbsentee->user_channel_id = userChannel($code_channel, user()->id)->id;
                $createNewAbsentee->code_absen = $createNewAbsentee->id . date("YmdHis");
                $createNewAbsentee->assign_to = $assignTo;
                $createNewAbsentee->save();

                try {
                    $title = strtolower("PRESENSI " . $event->name_activity);
                    $body = strtoupper("PRESENSI KEHADIRAN " . $event->name_activity . " TELAH DIBUKA OLEH " . user()->name . " AMAL SHOLEH SEGERA MERAPAT! ALHAMDULILLAH JAZA KUMULLOHU KHOIROO");
                    // $notifResponse = $this->sendNotification($title, $body, $code_channel);
                } catch (\Throwable $th) {
                }
                $response["message"] = "Berhasil membuka absensi untuk kegiatan {$event->name_activity}";
                userLogs(user()->name, "create", user()->name . " membuka absensi untuk kegiatan '{$event->name_activity}' pada " . Carbon::now(), $code_channel);
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function absenteeList(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search == "null" ? null : $request->search;
        $status = $request->status;
        $totalData = $request->total_data;
        $findData = Absentee::join("user_channels", "user_channels.id", "=", "absentees.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->join("activities_channels", "activities_channels.id", "=", "absentees.activity_channel_id")
            ->where("user_channels.code_channel", $code_channel);

        if ($request->start_date) {
            $startDate = date("Y-m-d H:i:s", strtotime($request->start_date . " 00:00:00"));
            $findData->where("absentees.created_at", ">=", $startDate);
        }

        if ($request->end_date) {
            $endDate = date("Y-m-d H:i:s", strtotime($request->end_date . " 23:59:59"));
            $findData->where("absentees.created_at", "<=", $endDate);
        }

        if ($status != "all") {
            $status = (int)$status;
            $findData->where("absentees.status", $status);
        }

        if ($search) {
            $findData->where("activities_channels.name_activity", "like", "%{$search}%");
        }

        if ($totalData != "all") {
            $findData->limit($totalData);
        }

        $data = $findData->select("users.name", "user_channels.user_id", "activities_channels.name_activity", "activities_channels.start_at", "activities_channels.end_at", "absentees.created_at", "absentees.status", "absentees.id", "absentees.code_absen", "absentees.total_absen")
            ->orderBy("absentees.created_at", "DESC")
            ->get();

        if (count($data)) {
            $response["data"] = $data;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function myAbsenteeList(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search == "null" ? null : $request->search;
        $status = $request->status;
        $totalData = $request->total_data;
        $date = date("Y-m-d", strtotime($request->date));
        $findData = AbsenteeUserChannel::join("absentees", "absentees.id", "absentee_user_channels.absentee_id")
            ->join("user_channels", "user_channels.id", "=", "absentee_user_channels.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->join("activities_channels", "activities_channels.id", "=", "absentees.activity_channel_id")
            ->where("user_channels.code_channel", $code_channel)
            ->where("users.id", user()->id)
            ->where("absentees.created_at", ">=", $date);

        if ($search) {
            $findData->where("activities_channels.name_activity", "like", "%{$search}%");
        }

        if ($totalData != "all") {
            $findData->limit($totalData);
        }

        $data = $findData->orderBy("activities_channels.id", "DESC")
            ->select("users.name", "activities_channels.name_activity", "absentee_user_channels.created_at")
            ->get();
        if (count($data)) {
            $response["data"] = $data;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function closeAbsentee(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $absenteeId = base64_decode($request->eventId);
        $findAbsentee = Absentee::find($absenteeId);
        if (!$findAbsentee) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Absentee not found";
        } else {
            $findAbsentee->status = 0;
            $findAbsentee->update();

            $response["message"] = "Absensi berhasil ditutup";
        }

        return response()->json($response, $response["code"]);
    }

    public function userAbsenteeEventIndex($code_channel, $code_absent)
    {
        $findAbsentee = Absentee::join("activities_channels", "activities_channels.id", "=", "absentees.activity_channel_id")
            ->join("user_channels", "user_channels.id", "=", "absentees.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentees.code_absen", $code_absent)
            ->select("users.name", "user_channels.user_id", "activities_channels.name_activity", "activities_channels.start_at", "activities_channels.end_at", "absentees.created_at", "absentees.status", "absentees.id", "absentees.code_absen")
            ->first();
        if (!$findAbsentee) {
            abort(404);
        }
        $findUserAbsentees = AbsenteeUserChannel::join("user_channels", "user_channels.id", "=", "absentee_user_channels.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentee_user_channels.absentee_id", $findAbsentee->id)
            ->select("users.name", "users.gender", "absentee_user_channels.*")
            ->orderBy("absentee_user_channels.id", "ASC")
            ->get();
        $listIdsUnactionUsers = collect($findUserAbsentees)->pluck('user_channel_id');
        $totalUsers = UserChannel::where("code_channel", $code_channel)->count();
        $userUnaction = UserChannel::with('user')->whereNotIn('id', $listIdsUnactionUsers->toArray())->get();
        $dataArr[] = "totalUsers";
        $dataArr[] = "userUnaction";
        $dataArr[] = "code_channel";
        $dataArr[] = "findAbsentee";
        $dataArr[] = "findUserAbsentees";
        return view("app.dashboard-channel.absentee", compact($dataArr));
    }

    public function userAbsenteeEventIndexPrint($code_channel, $code_absent)
    {
        $findAbsentee = Absentee::join("activities_channels", "activities_channels.id", "=", "absentees.activity_channel_id")
            ->join("user_channels", "user_channels.id", "=", "absentees.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentees.code_absen", $code_absent)
            ->select("users.name", "user_channels.user_id", "activities_channels.name_activity", "activities_channels.start_at", "activities_channels.end_at", "absentees.created_at", "absentees.status", "absentees.id", "absentees.code_absen")
            ->first();
        if (!$findAbsentee) {
            abort(404);
        }
        $findUserAbsentees = AbsenteeUserChannel::join("user_channels", "user_channels.id", "=", "absentee_user_channels.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentee_user_channels.absentee_id", $findAbsentee->id)
            ->select("users.name", "users.gender", "absentee_user_channels.created_at", "absentee_user_channels.id")
            ->orderBy("absentee_user_channels.id", "ASC")
            ->get();
        $dataArr[] = "code_channel";
        $dataArr[] = "findAbsentee";
        $dataArr[] = "findUserAbsentees";
        return view("app.dashboard-channel.print.list-absentees", compact($dataArr));
    }

    public function userAbsenteeEventIndexPrintQrcode($code_channel, $code_absent)
    {
        $findAbsentee = Absentee::join("activities_channels", "activities_channels.id", "=", "absentees.activity_channel_id")
            ->join("user_channels", "user_channels.id", "=", "absentees.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentees.code_absen", $code_absent)
            ->select("users.name", "user_channels.user_id", "activities_channels.name_activity", "activities_channels.start_at", "activities_channels.end_at", "absentees.created_at", "absentees.status", "absentees.id", "absentees.code_absen")
            ->first();
        if (!$findAbsentee) {
            abort(404);
        }
        $findUserAbsentees = AbsenteeUserChannel::join("user_channels", "user_channels.id", "=", "absentee_user_channels.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentee_user_channels.absentee_id", $findAbsentee->id)
            ->select("users.name", "users.gender", "absentee_user_channels.created_at", "absentee_user_channels.id")
            ->orderBy("absentee_user_channels.id", "ASC")
            ->get();
        $dataArr[] = "code_channel";
        $dataArr[] = "findAbsentee";
        $dataArr[] = "findUserAbsentees";
        return view("app.dashboard-channel.print.qrcode", compact($dataArr));
    }

    public function userLeaveAbsenteeEventView($code_channel, $code_absent)
    {
        $findUserChannelValid = UserChannel::where("code_channel", $code_channel)
                                        ->where("user_id", user()->id)
                                        ->first();
        if (!$findUserChannelValid) {
            abort(404);
        }

        $findAbsentee  = Absentee::with(['activity'])->where("code_absen", $code_absent)
                                ->where("status", 1)
                                ->first();

        if (!$findAbsentee) {
            abort(404);
        }

        $findAbsenteeUser = AbsenteeUserChannel::where("user_channel_id", $findUserChannelValid->id)
                                ->where("absentee_id", $findAbsentee->id)
                                ->first();
        if($findAbsenteeUser) {
            abort(404);
        }

        return view("app.dashboard-channel.form-leave", compact('findAbsentee', 'code_channel', 'code_absent'));
    }

    public function userAbsenteeEvent($code_channel, $code_absent)
    {
        $findUserChannelValid = UserChannel::where("code_channel", $code_channel)
            ->where("user_id", user()->id)
            ->first();

        if (!$findUserChannelValid) {
            abort(404);
        }

        $findAbsentee  = Absentee::with(['activity'])->where("code_absen", $code_absent)
            ->where("status", 1)
            ->first();
        if (!$findAbsentee) {
            abort(404);
        }

        $findAbsenteeUser = AbsenteeUserChannel::where("user_channel_id", $findUserChannelValid->id)
            ->where("absentee_id", $findAbsentee->id)
            ->first();
        if (!$findAbsenteeUser) {
            $createAbsentee = new AbsenteeUserChannel();
            $createAbsentee->user_channel_id = $findUserChannelValid->id;
            $createAbsentee->absentee_id = $findAbsentee->id;
            $createAbsentee->save();

            $findAbsentee->total_absen++;
            $findAbsentee->update();

            if($findUserChannelValid->type_user) {
                return redirect()->route("channel.absentee.users.index", [$code_channel, $code_absent])->with("success", "Anda berhasil absen");
            } else {
                return view('app.dashboard-channel.success-view', [
                    "img" => asset('assets/img/illustrations/success-absentee.png'),
                    "message" => "Anda berhasil absen di kegiatan ".$findAbsentee->activity->name_activity,
                ]);
            }
        }
        return redirect()->route("user.dashboard")->with("error", "Anda telah berhasil absen sebelumya");
    }

    public function userAbsenteeLeaveEvent(Request $request, $code_channel, $code_absent)
    {
        try {
            $request->validate([
                "reason" => ["required", "min:15"],
            ],[
                "reason.required" => "Alasan perizinan wajib di isi !",
                "reason.min" => "Alasan perizinan kamu terlalu pendek",
            ]);
        } catch (\Throwable $th) {
            return redirect()->back()->with("error", $th->getMessage())->withInput();
        }
        $findUserChannelValid = UserChannel::where("code_channel", $code_channel)
            ->where("user_id", user()->id)
            ->first();

        if (!$findUserChannelValid) {
            abort(404);
        }

        $findAbsentee  = Absentee::with(['activity'])->where("code_absen", $code_absent)
            ->where("status", 1)
            ->first();
        if (!$findAbsentee) {
            abort(404);
        }

        $findAbsenteeUser = AbsenteeUserChannel::where("user_channel_id", $findUserChannelValid->id)
            ->where("absentee_id", $findAbsentee->id)
            ->first();
        if (!$findAbsenteeUser) {
            $createAbsentee = new AbsenteeUserChannel();
            $createAbsentee->user_channel_id = $findUserChannelValid->id;
            $createAbsentee->absentee_id = $findAbsentee->id;
            $createAbsentee->reason = $request->reason;
            $createAbsentee->leave_category = $request->leave_category;
            $createAbsentee->type = "leave";
            $createAbsentee->save();

            $findAbsentee->total_leave++;
            $findAbsentee->update();

            if($findUserChannelValid->type_user) {
                return redirect()->route("channel.absentee.users.index", [$code_channel, $code_absent])->with("success", "Anda berhasil mengirimkan pesan perizinan");
            } else {
                return view('app.dashboard-channel.success-view', [
                    "img" => asset('assets/img/illustrations/success-absentee.png'),
                    "message" => "Anda berhasil mengirimkan pesan perizinan untuk kegiatan ".$findAbsentee->activity->name_activity,
                ]);
            }
        }
        return redirect()->route("user.dashboard")->with("error", "Anda telah berhasil absen sebelumya");
    }

    public function userAbsenteeEventList(Request $request, $code_channel, $absentId)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $lastId = $request->lastIdAbsent;
        $findUserAbsentees = AbsenteeUserChannel::join("user_channels", "user_channels.id", "=", "absentee_user_channels.user_channel_id")
            ->join("users", "users.id", "=", "user_channels.user_id")
            ->where("absentee_user_channels.absentee_id", $absentId)
            ->where("absentee_user_channels.id", ">", $lastId);

        $data = $findUserAbsentees->orderBy("absentee_user_channels.id", "ASC")
            ->select("users.name", "users.gender", "absentee_user_channels.created_at", "absentee_user_channels.id")
            ->get();

        if (count($data)) {
            $response["data"] = $data;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function deleteAbsentByUserChannelId($code_channel, $absenUserChannelId) {
        try {
            $findData = AbsenteeUserChannel::find($absenUserChannelId);
            if(!$findData) {
                throw new Exception('Data absen tidak valid!');
            }
            $findData->delete();
            return response()->json(['status' => true, 'message' => "Berhasil hapus data absensi"]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()], 500);
        }
    }

    public function manualAbsent(Request $request, $codeChannel, $absentId) {
        try {
            $request->validate([
                "leave_category" => ["required"],
                "user" => ["required", "array"],
                'user.*' => ['required'],
            ], [
                'user.*' => "Silahkan select user dahaulu!",
            ]);
            $findAbsentee = Absentee::find($absentId);
            if(!$findAbsentee) {
                throw new Exception("Data absent tidak valid");
            }
            $typeAbsent = $request->tipe;
            $listUsers = $request->user;
            $leaveCategory = $request->leave_category;
            $reason = $request->reason;
            $data = [];
            foreach ($listUsers as $userId) {
                $data[] = [
                    "user_channel_id" => $userId,
                    "absentee_id" => $absentId,
                ];
            }
            if($typeAbsent == 'present') {
                $dataRequest = collect($data)->map(function($arr) {
                    $arr['type'] = "present";
                    return $arr;
                });
                $findAbsentee->total_absen++;
                $findAbsentee->update();
            } else {
                $dataRequest = collect($data)->map(function($arr) use($reason, $leaveCategory) {
                    $arr['type'] = "leave";
                    $arr['leave_category'] = $leaveCategory;
                    $arr['reason'] = $reason ? '[ABSENT MANUAL]: '.$reason : "ABSENT MANUAL";
                    return $arr;
                });
                $findAbsentee->total_leave++;
                $findAbsentee->update();
            }
            AbsenteeUserChannel::insert($dataRequest->toArray());

            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => $th->getMessage()], 500);
        }
    }
}
