<?php

namespace App\Http\Controllers;

use App\Mail\SendCancelInvitationChannel;
use App\Models\Channel;
use App\Models\ChannelInvitation;
use App\Models\User;
use App\Models\UserChannel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Mail\SendInvitationChannel;
use App\Models\ActivitiesChannel;
use App\Models\Role;
use App\Models\UserChannelLogs;
use Illuminate\Support\Facades\Auth;

class ChannelController extends Controller
{   

    public function joinChannel(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $rules = [
            "data" => ["required"],
        ];

        $message =  [
            "data.required" => "Maaf silahkan isi nama channel!",
            "data.unique" => "Maaf nama channel telah digunakan!"
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            $checkCodeChannel = Channel::where("code_channel", $request->data)->first();
            if(!$checkCodeChannel) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Kode channel tidak valid!";
            } else {
                $findChannel = Channel::join("user_channels","user_channels.code_channel","=","channels.code_channel")
                                ->where("channels.code_channel", $request->data)
                                ->where("user_channels.user_id", user()->id)
                                ->select("user_channels.*","channels.channel_name")
                                ->first();
                if($findChannel) {
                    $response["code"] = 400;
                    $response["status"] = false;
                    $response["message"] = "Kamu sudah tergabung di channel ".$findChannel->channel_name;
                } else {
                    
                    $createUserChannel = new UserChannel();
                    $createUserChannel->code_channel = $request->data;
                    $createUserChannel->user_id = user()->id;
                    $createUserChannel->type_user = 0;
                    $createUserChannel->save();
                    
                    userLogs(user()->name, "join", user()->name." telah join channel pada ". Carbon::now(), $createUserChannel->code_channel);
                    // $sessionCodeChannel = session("code_channels");
                    // $sessionCodeChannel[] = $createUserChannel->code_channel;
                    // session(["code_channels" => $sessionCodeChannel]);
                    
        
                    $response["message"] = "Berhasil bergabung ke channel ".$checkCodeChannel->channel_name;
                    $response["data"] = $checkCodeChannel;
                }
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function createChannel(Request $request)
    {
        $response = [
            "code"  => 200,
            "status" => true,
            "data" => $request->all(),
        ];

        $rules = [
            "data" => ["required","unique:channels,channel_name"],
        ];

        $message =  [
            "data.required" => "Maaf silahkan isi nama channel!",
            "data.unique" => "Maaf nama channel telah digunakan!"
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            $createNewChannel = new Channel();
            $createNewChannel->code_channel = Str::random(6);
            $createNewChannel->channel_name = $request->data;
            $createNewChannel->save();

            $createUserChannel = new UserChannel();
            $createUserChannel->code_channel = $createNewChannel->code_channel;
            $createUserChannel->user_id = user()->id;
            $createUserChannel->save();

            $response["message"] = "Berhasil membuat channel baru";
            $response["data"] = $createNewChannel;
        }


        return response()->json($response, $response["code"]);
    }

    public function getAllChannelUser(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => false,
            "data" => $request->all()
        ];
        $offset = $request->offset ?? 0;
        $totalData = $request->total_data ?? 6;
        $getChannels = Channel::join("user_channels","user_channels.code_channel","=","channels.code_channel")
                        ->join("users","users.id","=","user_channels.user_id")
                        ->where("users.id", user()->id)
                        ->select("channels.*")
                        ->skip($offset)
                        ->take($totalData)
                        ->orderBy("created_at","DESC")
                        ->get();
        $response["data"] = $getChannels;

        return response()->json($response, $response["code"]);
    }

    public function dashboardChannel($code_channel)
    {
        $findChannel = Channel::where("code_channel", $code_channel)->first();
        if(!$findChannel) {
            abort(404);
        }
        $totalMember = UserChannel::where("code_channel", $code_channel)->count();
        $totalEvent = ActivitiesChannel::where("code_channel", $code_channel)->count();
        $dataArr = [];
        $roles = Role::orderBy("name", "ASC")->get();
        $statusInChannel =  UserChannel::where("code_channel", $code_channel)->where("user_id", Auth::id())->first();
        $dataCharts = [];
        foreach ($roles as $role) {
            $dataCharts[$role->name] = UserChannel::join("users","users.id","user_channels.user_id")
                                        ->where("user_channels.code_channel", $code_channel)
                                        ->where("users.role", $role->name)
                                        ->count();
        }
        session(["channel_name" => $findChannel->channel_name]);
        session(["role_channel" => $statusInChannel->type_user ? 'admin' : 'anggota']);
        $dataArr[] = "findChannel";
        $dataArr[] = "totalMember";
        $dataArr[] = "dataCharts";
        $dataArr[] = "totalEvent";
        $dataArr[] = "code_channel";
        
        return view("app.dashboard-channel.index", compact($dataArr));
    }

    public function listUserList(Request $request, $code_channel)
    {
        $return = array(
            'data' => [],
            'all' => 0,
            'iTotalRecords' => 0,
            'iTotalDisplayRecords' => 0,
            'sEcho' => (int) $request->input('sEcho', true)
        );

        $page = (!empty($request->input('iDisplayStart', true))) ? $request->input('iDisplayStart', true) : 0;
        $this->limit = (!empty($request->input('iDisplayLength', true))) ? $request->input('iDisplayLength', true) : 10;


        $sort_by = $request->input('iSortCol_0', true);
        switch ($sort_by) {
            case 0:
                $order_by = 'created_at';
                break;
            default:
                $order_by = 'created_at';
                break;
        }

        $order = $request->input('sSortDir_0', true);
        $search = $request->input('sSearch') ?? '';

        $users = User::join("user_channels","user_channels.user_id", "=","users.id")
                    ->where("user_channels.code_channel", $code_channel);

        if($search){
            // $mutasi
            // ->orWhere('amount', 'like', '%'.$search.'%')
            // ->orWhere('channel', 'like', '%'.$search.'%')
            // ->orWhere('phonenumber', 'like', '%'.$search.'%')
            // ->orWhere('sender', 'like', '%'.$search.'%')
            // ->orWhere('description', 'like', '%'.$search.'%')
            // ->orWhere('notes', 'like', '%'.$search.'%');
        }
        if (!empty($total_record = $users->count())) {
            $mutasiData = $users->orderBy("user_channels.created_at","DESC")
                    ->offset($page)
                    ->limit($this->limit)
                    ->get();

            $result = $mutasiData;

            $return['all'] = $return['iTotalRecords'] = $return['iTotalDisplayRecords'] = $total_record;
            $return['data'] = $result;
        }

        return response()->json($return, 200);
    }

    public function leaveChannel(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $userId = $request->user_id;

        $findUserChannel = UserChannel::where("code_channel", $code_channel)
                            ->where("user_id", $userId)
                            ->first();
        $findUserAdminChannel = UserChannel::where("user_id","!=", $userId)
                                ->where("code_channel", $code_channel)
                                ->where("type_user", 1)
                                ->count();
        if(!$findUserChannel) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Data user tidak ditemukan di channel ini (user not valid)";
        } else {
            if(!$findUserAdminChannel) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Jika ingin keluar channel silahkan berikan status admin ke user lain";
            } else {
                userLogs(user()->name, "leave", user()->name." telah meninggalkan channel pada ". Carbon::now(), $code_channel);
                // $sessionArr = sessionToArray(session("code_channels"));
                // $sessionArr = deleteKeyArr($sessionArr, $code_channel);
                // session(["code_channels" => $sessionArr]);
                $findUserChannel->delete();
                $response["message"] = "Berhasil keluar dari channel";
            }
        }

        return response()->json($response,$response["code"]);
    }

    public function linkInvitationChannel($random_code)
    {
        try {
            $randomCode = base64_decode($random_code);
            $findInvitation = ChannelInvitation::join("channels","channels.code_channel","=","channel_invitations.code_channel")
                                                ->where("random_code", $randomCode)->firstOrFail();
            if(user()->id != $findInvitation->user_id) {
                abort(404);
            } else if($findInvitation->status == 2) {
                return redirect()->route("user.dashboard")->with("errors","Undangan ke channel {$findInvitation->channel_name} telah dibatalkan!");
            }
            $findInvitation->status = 1;
            $createUserChannel = new UserChannel();
            $createUserChannel->code_channel = $findInvitation->code_channel;
            $createUserChannel->user_id = $findInvitation->user_id;
            $createUserChannel->type_user = 0;
            $createUserChannel->save();
            $findInvitation->update();

            userLogs(user()->name, "join", user()->name." telah bergabung ke channel pada ". Carbon::now(), $createUserChannel->code_channel);

            return redirect()->route("user.dashboard")->with("success","Anda berhasil bergadung di channel {$findInvitation->channel_name}");
        } catch (\Throwable $th) {
            abort(404);
        }
    }
    
}
