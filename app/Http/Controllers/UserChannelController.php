<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendCancelInvitationChannel;
use App\Models\Channel;
use App\Models\ChannelInvitation;
use App\Models\User;
use App\Models\UserChannel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Mail\SendInvitationChannel;
use App\Models\Role;
use App\Models\UserChannelLogs;
use Illuminate\Support\Facades\Auth;

class UserChannelController extends Controller
{
    public function listUserIndex($code_channel)
    {
        $dataArr = [];
        $dataArr[] = "code_channel";
        $findUserChannel = UserChannel::where("user_id", user()->id)
            ->where("code_channel", $code_channel)
            ->first();
        $roles = Role::orderBy("name", "ASC")->get();
        $dataArr[] = "findUserChannel";
        $dataArr[] = "roles";
        return view("app.dashboard-channel.daftar-user", compact($dataArr));
    }

    public function userLogIndex($code_channel)
    {
        $dataArr = [];
        $dataArr[] = "code_channel";
        return view("app.dashboard-channel.users-log", compact($dataArr));
    }

    public function listUserLogs(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search ?? null;
        $totalUser = $request->totalUser;
        $findUser = UserChannelLogs::join("channels", "channels.code_channel", "=", "user_channel_logs.code_channel")
            ->where("user_channel_logs.code_channel", $code_channel);

        if ($search) {
            $findUser->where("user_channel_logs.description", "like", "%{$search}%");
        }

        if ($totalUser != "all") {
            $findUser->limit($totalUser);
        }
        $users = $findUser->orderBy("id", "DESC")->get();
        if (count($users)) {
            $response["data"] = $users;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function cancelInviation(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => false,
            "data" => $request->all()
        ];

        $randomCode = $request->random_code;
        $findInvitation = ChannelInvitation::join("users", "users.id", "channel_invitations.user_id")
            ->join("channels", "channels.code_channel", "=", "channel_invitations.code_channel")
            ->where("random_code", $randomCode)
            ->first();
        if (!$findInvitation) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Channel tidak valid!";
        } else {
            if ($findInvitation->status == 2) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Pembatalan undangan telah dilakukan sebelumnya";
            } else {
                ChannelInvitation::where("random_code", $randomCode)
                    ->update([
                        "status" => 2
                    ]);
                $dataArr = [
                    "name" => $findInvitation->name,
                    "channel_name" => $findInvitation->channel_name,
                    "gender" => $findInvitation->gender,
                    "canceled_by" => user()->name,
                ];
                Mail::to($findInvitation->email)->send(new SendCancelInvitationChannel($dataArr));
                userLogs(user()->name, "join", user()->name . " telah melakukan pembatalan undangan channel untuk {$findInvitation->name} pada " . Carbon::now(), $code_channel);
                $response["message"] = "Berhasil membatalkan undangan channel";
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function inviteUserIndex($code_channel)
    {
        $dataArr[] = "code_channel";
        return view("app.dashboard-channel.invite-user", compact($dataArr));
    }

    public function listInvitations(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search == "null" ? null : $request->search;
        $totalData = $request->total_data;
        $status = $request->status;
        $findData = User::join("channel_invitations", "channel_invitations.user_id", "=", "users.id")
            ->where("channel_invitations.code_channel", $code_channel);

        if ($status != "all") {
            $findData->where("status", $status);
        }

        if ($search) {
            $findData->where("users.name", "like", "%{$search}%");
        }

        if ($totalData != "all") {
            $findData->limit($totalData);
        }

        $data = $findData->get();
        if (count($data)) {
            $response["data"] = $data;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function inviteUser(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $rules = [
            "userId" => ["required"],
            "type_inviation" => ["required"],
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response["code"]  = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            $userId = $request->userId;
            $typeInvitation = $request->type_inviation;

            $isMemberChannel = UserChannel::where("user_id", $userId)
                ->where("code_channel", $code_channel)
                ->count();
            if ($isMemberChannel) {
                $response["code"]  = 400;
                $response["status"] = false;
                $response["message"] = "User telah gabung di channel ini";
            } else {
                $findUser = User::find($userId);
                if (!$findUser) {
                    $response["code"]  = 400;
                    $response["status"] = false;
                    $response["message"] = "User tidak valid!";
                } else {
                    $findInviationExist = ChannelInvitation::join("users", "users.id", "=", "channel_invitations.user_id")
                        ->where("user_id", $userId)
                        ->where("code_channel", $code_channel)
                        ->where("type_invitation", $typeInvitation)
                        ->select("users.name", "users.email", "channel_invitations.*")
                        ->first();

                    if ($findInviationExist && ($sentNewInvitationAt = Carbon::parse($findInviationExist->created_at)->addDays(3)) > Carbon::now()) {
                        $lastSentInvitationAt = Date("d-m-Y H:i", strtotime($findInviationExist->created_at));
                        $response["code"]  = 400;
                        $response["status"] = false;
                        $response["message"] = "Maaf Anda telah mengirim undangan channel ke {$findUser->name} pada {$lastSentInvitationAt} via {$findInviationExist->type_invitation} Anda bisa mengirimkan undangan lagi pada {$sentNewInvitationAt->format("d-m-Y H:i")}";
                    } else {
                        $findChannel = Channel::where("code_channel", $code_channel)->first();
                        if (!$findChannel) {
                            $response["code"]  = 400;
                            $response["status"] = false;
                            $response["message"] = "Maaf channel tidak ditemukan";
                        } else if (!$findUser) {
                            $response["code"]  = 400;
                            $response["status"] = false;
                            $response["message"] = "Maaf user tidak ditemukan";
                        } else {
                            try {
                                DB::beginTransaction();
                                $createNewInviationChannel = new ChannelInvitation();
                                $createNewInviationChannel->code_channel = $code_channel;
                                $createNewInviationChannel->invited_by = user()->name;
                                $createNewInviationChannel->type_invitation = $typeInvitation;
                                $createNewInviationChannel->random_code = faker()->uuid;
                                $createNewInviationChannel->user_id = $userId;
                                $createNewInviationChannel->save();
                                if ($typeInvitation == "email") {
                                    $dataArr = [
                                        "name" => $findUser->name,
                                        "link_invitation" => route('channel.url.invitation', base64_encode($createNewInviationChannel->random_code)),
                                        "channel_name" => $findChannel->channel_name,
                                        "gender" => $findUser->gender,
                                        "invited_by" => $createNewInviationChannel->invited_by,
                                    ];
                                    Mail::to($findUser->email)->send(new SendInvitationChannel($dataArr));
                                    userLogs(user()->name, "create", user()->name . " telah mengundang " . $findUser->name . " via email pada " . Carbon::now(), $code_channel);
                                }


                                $response["message"] = "Berhasil mengirim undangan channel ke {$findUser->name} via {$typeInvitation}";

                                DB::commit();
                            } catch (\Throwable $th) {
                                DB::rollback();
                                $response["code"]  = 400;
                                $response["status"] = false;
                                $response["message"] = $th->getMessage();
                            }
                        }
                    }
                }
            }
        }
        return response()->json($response, $response["code"]);
    }

    public function kickUser(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $userId = $request->user_id;

        $findUserChannel = UserChannel::where("code_channel", $code_channel)
            ->where("user_id", $userId)
            ->first();
        if (!$findUserChannel) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Data user tidak ditemukan di channel ini (user not valid)";
        } else {
            $findUserChannel->delete();
            $response["message"] = "Berhasil mengerluarkan {$request->username} dari channel";
            userLogs(user()->name, "kick", user()->name . " telah mengeluarkan " . $request->username . " dari channel pada " . Carbon::now(), $code_channel);
        }

        return response()->json($response, $response["code"]);
    }

    public function changeStatus(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $userId = $request->user_id;

        $findUserChannel = UserChannel::join("users", "users.id", "=", "user_channels.user_id")
            ->where("users.id", $userId)
            ->where("user_channels.code_channel", $code_channel)
            ->first();
        if (!$findUserChannel) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Data user tidak ditemukan di channel ini (user not valid)";
        } else {
            UserChannel::where("user_id", $findUserChannel->user_id)
                ->where("code_channel", $code_channel)
                ->update([
                    "type_user" => $findUserChannel->type_user == 1 ? 0 : 1
                ]);
            userLogs(user()->name, "leave", $findUserChannel->name . " telah dijadikan admin oleh " . user()->name . " channel pada " . Carbon::now(), $code_channel);

            $response["message"] = "Berhasil mengubah {$findUserChannel->name} menjadi " . ($findUserChannel->type_user == 1 ? "admin" : "anggota");
        }


        return response()->json($response, $response["code"]);
    }

    public function getListUser(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search ?? null;
        $totalUser = $request->totalUser;
        $gender = $request->gender;
        $category = $request->category;
        $findUser = User::join("user_channels", "user_channels.user_id", "=", "users.id")
            ->where("user_channels.code_channel", $code_channel);

        if ($gender != "all") {
            $findUser->where("users.gender", $gender);
        }

        if ($category != "all") {
            $findUser->where("users.role", $category);
        }

        if ($search) {
            $findUser->where("users.name", "like", "%{$search}%");
        }

        if ($totalUser != "all") {
            $findUser->limit($totalUser);
        }

        $users = $findUser->get();
        if (count($users)) {
            $response["data"] = $users;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function searchingUser(Request $request, $code_channel, $username)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $findUser = User::where("name", "like", "%{$username}%");


        $users = $findUser->limit(5)->get();
        if (count($users)) {
            $response["data"] = $users;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }
}
