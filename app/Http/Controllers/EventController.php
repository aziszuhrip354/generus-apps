<?php

namespace App\Http\Controllers;

use App\Models\ActivitiesChannel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function listEventIndex($code_channel)
    {
        $dataArr[] = "code_channel";
        return view("app.dashboard-channel.event", compact($dataArr));
    }

    public function createEvent(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $rules = [
            "name" => ["required"],
            "startAt" => ["required"],
            "endAt" => ["required"],
        ];

        $message = [
            "name.required" => "Nama kegiatan harus diisi!",
            "startAt.required" => "Waktu mulai kegiatan harus diisi!",
            "endAt.required" => "Waktu selesai kegiatan harus diisi!",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {    
            $findEventSame = ActivitiesChannel::where("code_channel", $code_channel)
                                                ->where("name_activity","like", $request->name)
                                                ->count();
            if($findEventSame) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Kegiatan telah ada di channel ini sebelumnya";
            } else {
                $createNewEvent = new ActivitiesChannel();
                $createNewEvent->code_channel = $code_channel;
                $createNewEvent->name_activity = $request->name;
                $createNewEvent->start_at = $request->startAt;
                $createNewEvent->end_at = $request->endAt;
                $createNewEvent->save();
    
                $response["message"] = "Berhasil membuat kegiatan baru";
                userLogs(user()->name, "create", user()->name." membuat kegiatan baru '{$createNewEvent->name_activity}' pada ". Carbon::now(), $code_channel);
            }

        }

        return response()->json($response, $response["code"]);
    }

    public function editEvent(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $rules = [
            "name" => ["required"],
            "startAt" => ["required"],
            "endAt" => ["required"],
        ];

        $message = [
            "name.required" => "Nama kegiatan harus diisi!",
            "startAt.required" => "Waktu mulai kegiatan harus diisi!",
            "endAt.required" => "Waktu selesai kegiatan harus diisi!",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = $validator->errors()->first();
        } else {
            $findEvent = ActivitiesChannel::where("code_channel", $code_channel)
                                            ->where("id", base64_decode($request->eventId))
                                            ->first();
            if(!$findEvent) {
                $response["code"] = 400;
                $response["status"] = false;
                $response["message"] = "Event not found!";
            } else {
                $findEvent->code_channel = $code_channel;
                $findEvent->name_activity = $request->name;
                $findEvent->start_at = $request->startAt;
                $findEvent->end_at = $request->endAt;
                $findEvent->update();
                
                userLogs(user()->name, "update-status", user()->name." mengupdate kegiatan '{$findEvent->name_activity}' pada ". Carbon::now(), $code_channel);
                $response["message"] = "Berhasil mengupdate kegiatan";
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function listEvent(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "data" => $request->all()
        ];

        $search = $request->search == "null" ? null : $request->search; 
        $totalData = $request->total_data;
        $findData = ActivitiesChannel::where("code_channel", $code_channel);

        if($search) {
            $findData->where("name_activity","like","%{$search}%");
        }
        
        if($totalData != "all") {
            $findData->limit($totalData);          
        }

        $data = $findData->orderBy("id","DESC")
                         ->get();
        if(count($data)) {
            $response["data"] = $data;
            $response["message"] = "Data berhasil diget";
        } else {
            $response["data"] = null;
            $response["message"] = "Data kosong";
        }
        return response()->json($response, $response["code"]);
    }

    public function deleteEvent(Request $request, $code_channel)
    {
        $response = [
            "code" => 200,
            "status" => false,
        ];

        $eventId = base64_decode($request->eventId);
        $findEventById = ActivitiesChannel::find($eventId);
        if(!$findEventById) {
            $response["code"] = 400;
            $response["status"] = false;
            $response["message"] = "Event not found!";
        } else {
            userLogs(user()->name, "leave", user()->name." menghapus kegiatan '{$findEventById->name_activity}' pada ". Carbon::now(), $code_channel);
            $findEventById->delete();
            $response["message"] = "Berhasil menghapus kegiatan";
        }

        return response()->json($response, $response["code"]);
    }
}
