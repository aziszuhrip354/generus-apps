<?php

namespace App\Http\Controllers;

use App\Mail\NotificationEmailVerification;
use App\Models\Link;
use App\Models\User;
use App\Models\UserChannel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use App\Models\Role;
use App\Services\EmailService\EmailServiceInterface;
use Exception;

class AuthController extends Controller
{

    private $emailService;

    public function __construct(
        EmailServiceInterface $emailService,
    )
    {
        $this->emailService = $emailService;
    }
    
    public function loginIndex()
    {
        if (user()) {
            return redirect()->route("user.dashboard");
        }
        return view("app.auth.auth-login-basic");
    }

    public function login()
    {
        return "";
    }

    public function registerIndex()
    {
        return view("app.auth.auth-register-basic", [
            "roles" => Role::all(),
        ]);
    }

    public function forgotPasswordIndex()
    {
        return view("app.auth.forgot-password");
    }

    public function verifyPassword(Request $request)
    {
        $response = [
            "status" => true,
        ];

        $password = $request->value;
        $regex =  "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/";
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $spesialChar = preg_match($regex, $password);
        if (!$uppercase) {
            $response["message"] = "Password Anda harus memuat huruf besar";
            $response["status"] = false;
        } else if (!$lowercase) {
            $response["message"] = "Password Anda harus memuat huruf kecil";
            $response["status"] = false;
        } else if (!$number) {
            $response["message"] = "Password Anda harus memuat angka";
            $response["status"] = false;
        } else if (!$spesialChar) {
            $response["message"] = "Password Anda harus memuat spesial karakter (!@#$%^&*)";
            $response["status"] = false;
        } else {
            $response["message"] = "Password sudah kuat...";
        }

        return response()->json($response);
    }

    public function register(Request $request)
    {
        try {
            $rules = [
                "email" => ["required", 'email:dns', 'unique:users,email'],
                "confirm_password" => ["required"],
                "password" => ["required"],
                "name" => ["required"],
                "no_hp" => ["required", "min:10"],
                "gender" => ["required"],
                "birthday" => ["required"],
            ];

            $message = [
                "email.required" => "Email tidak boleh kosong!",
                "email.email" => "Email Anda tidak valid!",
                "email.unique" => "Email Anda telah digunakan!",
                "confirm_password.required" => "Konfirmasi password tidak boleh kosong!",
                "password.required" => "Password tidak boleh kosong!",
                "no_hp.required" => "Nomor HP tidak boleh kosong!",
                "no_hp.min" => "Nomor HP terlalu pendek!",
                "name.required" => "Nama tidak boleh kosong!",
                "birthday.required" => "Tanggal lahir tidak boleh kosong!",
            ];

            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {
                $password = $request->password;
                $confirmPassword = $request->confirm_password;
                $name = $request->name;
                $email = $request->email;
                $noHp = $request->no_hp;
                $gender = $request->gender;
                $role = $request->role;
                $birthday = date("Y-m-d", strtotime($request->birthday));

                if ($password != $confirmPassword) {
                    return redirect()->back()->withErrors("Password dan konfirmasi password tidak cocok!")->withInput();
                } else {
                    $nomorHp = convertNoHpTo62($noHp);
                    if ($nomorHp == false) {
                        return redirect()->back()->withErrors("Nomor HP Anda tidak valid! (digit pertama harus 0 atau 62)")->withInput();
                    } else {
                        $createNewUser = new User();
                        $createNewUser->name = $name;
                        $createNewUser->email = $email;
                        $createNewUser->no_hp = $nomorHp;
                        $createNewUser->password = bcrypt($password);
                        $createNewUser->uuid = faker()->uuid;
                        $createNewUser->gender = $gender;
                        $createNewUser->role = $role;
                        $createNewUser->birthday = $birthday;
                        $createNewUser->save();

                        $dataArr = [
                            "name" => $createNewUser->name,
                            "gender" => $createNewUser->gender,
                            "email" => $createNewUser->email,
                            // "password" => $password,
                            "link_verification" => route('auth.verify-email', base64_encode($createNewUser->uuid)),
                        ];

                        Mail::to($email)->send(new NotificationEmailVerification($dataArr));
                    }
                }

                return redirect()->route('auth.login')->with("success", "Berhasil register di generus-apps silahkan check email Anda");
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with("errors", $th->getMessage());
        }
    }

    public function verifyEmail($encryptedCode)
    {
        try {
            $decryptedCode = base64_decode($encryptedCode);
            $findUser = User::where("uuid", $decryptedCode)->first();
            if (!$findUser) {
                abort(500);
            } else {
                $findUser->email_verified_at = Carbon::now();
                $findUser->update();
                return redirect()
                    ->route("auth.login")
                    ->with("success", "Email {$findUser->email} telah berhasil di verifikasi");
            }
        } catch (\Throwable $th) {
            abort(500);
        }
        abort(500);
    }

    public function generateLinkForgotPassword(Request $request) {
        try {
            $request->validate([
                "email" => ["required", "email:email"],
            ], [
                "email.required" => "Email wajib di isi ya!",
                "email.email" => "Mohon maaf email tidak valid",
            ]);

            $findEmail = User::where("email", $request->email)->first();

            if(!$findEmail) {
                throw new Exception("Maaf email ini salah (tidak terdaftar) !");
            }
            
            $expiredTime = 5;
            $newLinks = new Link();
            $newLinks->user_id = $findEmail->id;
            $newLinks->code_link = \faker()->uuid();
            $newLinks->expired_at = Carbon::now()->addMinutes($expiredTime);
            $newLinks->save();

            $this->emailService->sendEmail([
                "receiver" => $findEmail->email,
                "subject" => "Generus Apps: Link Reset Password",
                "view" => "email.email-forgot-password",
                "withData" => [
                    "expiredTime" => $expiredTime,
                    "link" => route('forgot.password.link', $newLinks->code_link),
                    "gender" => $findEmail->gender,
                    "name" => $findEmail->name,
                ],
            ]);
            return redirect()->back()->with("success", "Berhasil mengirimkan link reset password, silahkan cek emailnya");
        } catch (\Throwable $th) {
            return redirect()->back()->with("errors", $th->getMessage());
        }
    }

    public function linkResetPassowrd($codeLink) {
        try {
            $findLink = Link::with("user")->where("code_link", $codeLink)->firstOrFail();
            $expiredDate = Carbon::parse($findLink->expired_at);
            $now = Carbon::now();
            if($expiredDate < $now) {
                \abort(404);
            }
            return view("app.auth.reset-password", compact('findLink'));
        } catch (\Throwable $th) {
            abort(500);
        }
    }

    public function postResetPassowrd(Request $request, $codeLink) {
        try {
            $findLink = Link::with("user")->where("code_link", $codeLink)->firstOrFail();
            if($request->password != $request->confirm_password) {
                return \redirect()->back()->with("errors","Password konfirmasi tidak cocok!");
            }
            $findLink->user->update([
                "password" => \bcrypt($request->password),
            ]);

            $findLink->delete();

            return redirect()->route('auth.login')->with("success", "Berhasil reset password Anda, silahkan coba login");
        } catch (\Throwable $th) {
            abort(500);
        }
    }
    
    public function loginPost(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $checkCredentialsInDb = User::where("email", $email)->first();
        if (!$checkCredentialsInDb) {
            return redirect()
                ->back()
                ->with("errors", "Maaf email Anda tidak terdaftar!");
        } else {
            if (!$checkCredentialsInDb->email_verified_at) {
                $dataArr = [
                    "name" => $checkCredentialsInDb->name,
                    "gender" => $checkCredentialsInDb->gender,
                    "email" => $checkCredentialsInDb->email,
                    "link_verification" => route('auth.verify-email', base64_encode($checkCredentialsInDb->uuid)),
                ];
                Mail::to($email)->send(new NotificationEmailVerification($dataArr));
                return redirect()->back()->with("errors", "Hallo, {$checkCredentialsInDb->name} akun kamu belum verifikasi email. Yuk cek emailnya :').")->withInput();
            } else {
                $credentials = $request->validate([
                    "email" => ["required", "email:dns"],
                    "password" => ["required"],
                ]);
                $checkAuth = Auth::attempt($credentials, true);
                if ($checkAuth) {
                    $checkHashPassword = Hash::check($password, $checkCredentialsInDb->password);
                    if (!$checkHashPassword) {
                        return redirect()
                            ->back()
                            ->with("errors", "Maaf email atau password Anda salah!")
                            ->withInput();
                    } else {
                        $request->session()->regenerate();
                        return redirect()->intended("user/dashboard");
                    }
                } else {
                    return  redirect()
                        ->back()
                        ->with("errors", "Maaf email atau password Anda salah!")
                        ->withInput();
                }
            }
        }
    }
}
