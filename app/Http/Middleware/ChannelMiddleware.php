<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Models\UserChannel;
use Closure;
use Illuminate\Http\Request;

class ChannelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $paramCodeChannel = $request->route()->parameter("code_channel");
        $checkValidation = UserChannel::where("code_channel", $paramCodeChannel)
                                ->where("user_id", user()->id)
                                ->first();
        if(!$checkValidation) {
            abort(404);
        }
        return $next($request);
    }
}
