<footer class="content-footer footer bg-footer-theme">
    <div class="container-xxl d-flex justify-content-center">
      <div class="mb-4 mb-md-0 text-center" style="margin-bottom: 14px!important">
        Hak Cipta Generus Apps ©
        <script>
          document.write(new Date().getFullYear());
        </script>
        | made with ❤️ by
        <a href="https://www.linkedin.com/in/azis-zuhri-pratomo/" target="_blank" class="footer-link fw-bolder">Azis Zuhri Pratomo</a>
      </div>
    </div>
  </footer>