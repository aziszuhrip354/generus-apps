
<!DOCTYPE html>

<!-- beautify ignore:start -->
<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title> @yield("title") | Generus Apps</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="../assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <link rel="stylesheet" href="../assets/vendor/libs/apex-charts/apex-charts.css" />
    <style>

      .layout-menu-fixed:not(.layout-menu-collapsed) .layout-page, .layout-menu-fixed-offcanvas:not(.layout-menu-collapsed) .layout-page {
          padding-left: 0;
      }

      .scroll-container {
          height: 600px;
          overflow-y: scroll
      }
      
      .hollowLoader {
        width: 3em;
        height: 3em;
        -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
                animation: loaderAnim 1.25s infinite ease-in-out;
        outline: 1px solid transparent;
      }
      .hollowLoader .largeBox {
        height: 3em;
        width: 3em;
        background-color: #6a6cfe52;
        outline: 1px solid transparent;
      }
      .hollowLoader .smallBox {
        height: 3em;
        width: 3em;
        background-color: #6a6cfe;
        z-index: 1;
        outline: 1px solid transparent;
        -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
                animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
      }

      @-webkit-keyframes smallBoxAnim {
        0% {
          transform: scale(0.2);
        }
        100% {
          transform: scale(0.75);
        }
      }

      @keyframes smallBoxAnim {
        0% {
          transform: scale(0.2);
        }
        100% {
          transform: scale(0.75);
        }
      }
      @-webkit-keyframes loaderAnim {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }
      @keyframes loaderAnim {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }

      .main-container-loading {
          position: fixed;
          top: 0;
          width: 100%;
          height: 100%;
          background-color: #3232322b;
          z-index: 9999999999999999999999;
          justify-content: center;
          align-items: center;
          display: flex;
      }
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }

      .container-loading {
          position: absolute;
          width: 100%;
          height: 100%;
          background-color: #3232322b;
          z-index: 99;
          justify-content: center;
          align-items: center;
          display: flex;
      }
    </style>
    @yield("css")
    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="../assets/vendor/js/helpers.js"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="../assets/js/config.js"></script>
  </head>

  <body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
      <div class="layout-container">
        <!-- Menu -->

        {{-- @include('components.sidebar-user') --}}
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
          <!-- Navbar -->

          @include('components.navbar')
          <!-- / Navbar -->

          <!-- Content wrapper -->
          <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
              @yield('content')
            </div>
            <!-- / Content -->

            <!-- Footer -->
            @include("components.footer")
            <!-- / Footer -->

            <div class="content-backdrop fade"></div>
          </div>
          <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
      </div>

      <!-- Overlay -->
      <div class="layout-overlay layout-menu-toggle"></div>
    </div>

    <!-- Main Container loading -->
    <div class="main-container-loading" id="main-container-loading">
        <div class="">
            <div class="mb-4 fw-bold text-primary">
                <span>LOADING...</span>
            </div>
            <div class="" style="margin-left: 15px;transform: scale(1.5);">
                <div class="hollowLoader">
                  <div class="largeBox">
                    <div class="smallBox"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <!-- / Layout wrapper -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="../js/main.js"></script>
    <script src="../js/swal.js"></script>
    <script src="../js/bootstrap5.js"></script>
    <script src="../assets/vendor/libs/jquery/jquery.js"></script>
    <script src="../assets/vendor/libs/popper/popper.js"></script>
    <script src="../assets/vendor/js/bootstrap.js"></script>
    <script src="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="../assets/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="../assets/vendor/libs/apex-charts/apexcharts.js"></script>

    <!-- Main JS -->
    <script src="../assets/js/main.js"></script>

    <!-- Page JS -->
    <script src="../assets/js/dashboards-analytics.js"></script>

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>
    {{-- <script src="{{asset('js/firebase-messaging-sw.js')}}"></script> --}}
    <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script>
    <script>
    // var firebaseConfig = {
    //   apiKey: "AIzaSyBK2-axLKjIGgluTGKlBHgUoU5wznDJPh0",
    //   authDomain: "generus-apps.firebaseapp.com",
    //   projectId: "generus-apps",
    //   storageBucket: "generus-apps.appspot.com",
    //   messagingSenderId: "517210157328",
    //   appId: "1:517210157328:web:4724316c1be2bba3577595"
    // };
      
    // firebase.initializeApp(firebaseConfig);
    // const messaging = firebase.messaging();
  
    // function initFirebaseMessagingRegistration() {
    //       try {
    //         messaging
    //         .requestPermission()
    //         .then(function () {
    //             return messaging.getToken()
    //         })
    //         .then(function(token) {
    //             console.log(token);
   
    //             $.ajaxSetup({
    //                 headers: {
    //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                 }
    //             });
  
    //             $.ajax({
    //                 url: '{{ route("store.token") }}',
    //                 type: 'POST',
    //                 data: {
    //                     token: token
    //                 },
    //                 dataType: 'JSON',
    //                 success: function (response) {
    //                     console.log('Token saved successfully.');
    //                 },
    //                 error: function (err) {
    //                     console.log('User Chat Token Error'+ err);
    //                 },
    //             });
  
    //         }).catch(function (err) {
    //             console.log('User Chat Token Error'+ err);
    //         });
    //       } catch (error) {
    //         console.log(error);
    //       }
    //  }  
     
    //  try {
    //    initFirebaseMessagingRegistration();
    //    messaging.onMessage(function(payload) {
    //        const noteTitle = payload.notification.title;
    //        const noteOptions = {
    //            body: payload.notification.body,
    //            icon: payload.notification.icon,
    //        };
    //        new Notification(noteTitle, noteOptions);
    //    });
    //  } catch (error) {
    //    console.log(error);
    //  }


    $.ajaxSetup({
      'headers': {
          'X-CSRF-TOKEN': '{{csrf_token()}}'
      }
    })
    
    var $loading = $('#main-container-loading').hide();
      $(document)
          .ajaxStart(function () {
              $loading.show();
          })
          .ajaxStop(function () {
              $loading.hide();
      });


    const logout = () => {
      Swal.fire({
          title: 'Pesan!',
          text: "Apakah Anda ingin logout?",
          icon: 'warning',
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
      }).then(val => {
        if(val.isConfirmed) {
          let form = document.getElementById("formLogout");
          form.submit();
        }
      });
    }

    @if(session("errors"))
    Swal.fire({
      title: 'Pesan!',
      text: "{{session('errors')}}",
      icon: 'error',
      showCancelButton: false,
      showConfirmButton: false,
      timer: 2000
    });
    @endif
    @if(session("success"))
      Swal.fire({
        title: 'Pesan!',
        text: "{{session('success')}}",
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 3000
      });
    @endif

   
    </script>
    @yield("js")
  </body>
</html>
