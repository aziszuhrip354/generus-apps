<!DOCTYPE html>

<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="{{url('/')}}/assets/" data-template="vertical-menu-template-free">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title> Form Perizininan | {{ $findAbsentee->activity->name_activity }}</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/assets/img/favicon/favicon.ico" />


    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}"
        class="template-customizer-theme-css" />

    <style>
        form {
            width: 50%;
        }

        .bouncing-image {
            width: 100px;
            animation: bounce 2s ease-in-out infinite;
            /* Animasi naik turun */
        }

        /* Keyframes untuk animasi naik turun */
        @keyframes bounce {

            0%,
            100% {
                transform: translateY(0);
                /* Posisi awal dan akhir */
            }

            50% {
                transform: translateY(-20px);
                /* Posisi naik */
            }
        }

        /* Animasi terbang */
        @keyframes fly {
            0% {
                transform: translate(0, 0) rotate(0deg);
            }

            25% {
                transform: translate(300px, -50px) rotate(10deg);
            }

            50% {
                transform: translate(600px, 50px) rotate(-10deg);
            }

            75% {
                transform: translate(900px, -100px) rotate(15deg);
            }

            100% {
                transform: translate(1200px, 0) rotate(0deg);
            }
        }

        @media (max-width: 500px) {
            form {
                width: 100%;
            }

            #illustration {
                width: 60%!important;
            }
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row" style="min-height: 100vh">
            <div class="col-md-12 d-flex justify-content-center align-items-center">
                <form class="card rounded-2 border border-2 p-4" action="{{route('channel.absentee.leave',[$code_channel, $code_absent])}}" method="POST">
                    @csrf
                    <p class="text-center text-xl fw-bold text-primary text-uppercase">Form Perizinan</p>
                    <div class="text-center">
                        <img class="bouncing-image" id="illustration" style="width: 40%"
                            src="{{ asset('assets/img/illustrations/sent-reason.png') }}" alt="">
                    </div>
                    <div class="mb-3">
                        <label for="event" class="form-label fw-bold text-primary">Kegiatan</label>
                        <input type="text" class="form-control" readonly id="event"
                            value="{{ $findAbsentee->activity->name_activity }}">
                    </div>
                    <div class="mb-3">
                        <label for="leave_category" class="form-label fw-bold text-primary">Kategori Udzur</label>
                       <select name="leave_category" class="form-control" id="leave_category">
                            <option value="Sakit">Sakit</option>
                            <option value="diluar_kota">Diluar Kota</option>
                            <option value="Kerja">Kerja</option>
                            <option value="kegiatan_kuliah">Kegiatan Kuliah</option>
                            <option value="lainnya">Lainnya</option>
                       </select>
                    </div>
                    <div class="mb-3">
                        <label for="reason" class="form-label fw-bold text-primary">Keterangan Izin (Pesan)</label>
                        <textarea name="reason" id="reason" class="form-control" cols="10" rows="5"
                            placeholder="Masukan alasan Anda...">{{old("reason")}}</textarea>
                        @if (session('error'))
                            <small class="text-danger mt-3 text-xs fw-bold">{{session('error')}}</small>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary">Kirim</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('/js/swal.js') }}"></script>
    <script src="{{ asset('/js/bootstrap5.js') }}"></script>
    <script src="{{ asset('/assets/vendor/libs/jquery/jquery.js') }}"></script>

    <script>
        $.ajaxSetup({
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    </script>
    @yield('js')
</body>

</html>
