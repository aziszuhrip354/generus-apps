
<!DOCTYPE html>

<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title> List Absen | {{$findAbsentee->name_activity}}</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />


    <link rel="stylesheet"  media="print" href="{{asset("assets/css/bootstrap5.css")}}" />
    <link rel="stylesheet"  media="print" href="{{asset("assets/vendor/fonts/boxicons.css")}}" />

    <style>
        .container-head {
            width: 220px;
            margin: 0 auto;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0.5rem 5.2rem;
            border: 2px solid #696cff;
        }
    </style>
  </head>

  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="my-2">
                    <p class=" m-1 text-center">Absensi Kegiatan :</p>
                    <p class="fw-bold  m-1 text-center">{{$findAbsentee->name_activity}}</p>
                </div>
                <p class=" m-1 text-center">QR Code :</p>
                <div class="container-head card">
                    <div id="qrcode"></div>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Tanggal Kegiatan :</p>
                    <p class="fw-bold  m-1 text-center">{{date("d-M-Y", strtotime($findAbsentee->created_at))}}</p>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Dibuka Oleh :</p>
                    <p class="fw-bold  m-1 text-center">{{$findAbsentee->name}}</p>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Total Pengabsen :</p>
                    <p class="fw-bold  m-1 text-center" id="totalAbsentee">{{count($findUserAbsentees)}} Orang</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-6" id="man">
            <p class="text-center fw-bold text-uppercase">Daftar Hadir Laki-LAKI</p>
            <table class="w-100 my-3 table table-striped" id="tableMan">
                <tr>
                    <td class="text-center">#</td>
                    <td class="text-center">NAMA</td>
                    <td class="text-center">JAM HADIR</td>
                </tr>
                @php
                    $counterMan = 1;
                    $counterWomen = 1;
                    $lastIdAbsent = 0;
                @endphp
                @foreach ($findUserAbsentees as $absent)
                    @if($absent->gender == "pria")
                        <tr>
                            <td class="text-center">{{$counterMan++}}</td>
                            <td class="text-center">{{$absent->name}}</td>
                            <td class="text-center">{{date("d-m-Y H:i", strtotime($absent->created_at))}}</td>
                        </tr>
                    @endif
                    @php
                        $lastIdAbsent = $absent->id;
                    @endphp
                @endforeach
            </table>
        </div>
        <div class="col-md-6" id="women">
            <p class="text-center fw-bold text-uppercase">Daftar Hadir PEREMPUAN</p>
            <table class="w-100 my-3 table table-striped" id="tableWomen">
                <tr>
                    <td class="text-center">#</td>
                    <td class="text-center">NAMA</td>
                    <td class="text-center">JAM HADIR</td>
                </tr>
                @foreach ($findUserAbsentees as $absent)
                    @if($absent->gender == "wanita")
                        <tr>
                            <td class="text-center">{{$counterWomen++}}</td>
                            <td class="text-center">{{$absent->name}}</td>
                            <td class="text-center">{{date("d-m-Y H:i", strtotime($absent->created_at))}}</td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{asset("/js/qrcode.js")}}"></script>
    <script src="{{asset("/js/swal.js")}}"></script>
    <script src="{{asset("/js/bootstrap5.js")}}"></script>
    <script src="{{asset("/assets/vendor/libs/jquery/jquery.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js" integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        var qr = document.getElementById("qrcode");
      var qrcode = new QRCode(qr,{
            text: "{{route('channel.absentee.users',[$code_channel, $findAbsentee->code_absen])}}",
            width: 200,
            height: 200,
            colorDark : "#696cff",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
    });
    </script>
  </body>
</html>
