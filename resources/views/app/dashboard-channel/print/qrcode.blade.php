
<!DOCTYPE html>

<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title> List Absen | {{$findAbsentee->name_activity}}</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />


    <link rel="stylesheet"  media="print" href="{{asset("assets/css/bootstrap5.css")}}" />
    <link rel="stylesheet"  media="print" href="{{asset("assets/vendor/fonts/boxicons.css")}}" />

    <style>
        .container-head {
            width: 220px;
            margin: 0 auto;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0.5rem 5.2rem;
            border: 2px solid #696cff;
        }

        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
    </style>
  </head>

  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="my-2">
                    <h2 class=" m-1 text-center">Absensi Kegiatan :</h2>
                    <h2 class="fw-bold  m-1 text-center">{{$findAbsentee->name_activity}}</h2>
                </div>
                <h3 class=" m-1 text-center">QR Code :</h3>
                <div class="container-head card">
                    <div id="qrcode"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{asset("/js/qrcode.js")}}"></script>
    <script src="{{asset("/js/swal.js")}}"></script>
    <script src="{{asset("/js/bootstrap5.js")}}"></script>
    <script src="{{asset("/assets/vendor/libs/jquery/jquery.js")}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js" integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        var qr = document.getElementById("qrcode");
      var qrcode = new QRCode(qr,{
            text: "{{route('channel.absentee.users',[$code_channel, $findAbsentee->code_absen])}}",
            width: 200,
            height: 200,
            colorDark : "#696cff",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H
    });
    </script>
  </body>
</html>
