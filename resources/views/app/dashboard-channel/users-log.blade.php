@extends('template.master-with-sidebar')

@section("title")
 Log User    
@endsection

@section("css")
<style>
    .font-07 {
        font-size: 0.7rem;
    }

    #container-users {
        display: flex;
        overflow-x: hidden
    }

    .users {
    margin-bottom: 3px;
    border-radius: 10px;
    flex: 0 0 100%;
 }

 .scroll-container {
     height: 600px;
     overflow-y: scroll
 }
 
 .hollowLoader {
  width: 3em;
  height: 3em;
  -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
          animation: loaderAnim 1.25s infinite ease-in-out;
  outline: 1px solid transparent;
}
.hollowLoader .largeBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe52;
  outline: 1px solid transparent;
}
.hollowLoader .smallBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe;
  z-index: 1;
  outline: 1px solid transparent;
  -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
          animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
}

@-webkit-keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}

@keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}
@-webkit-keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}
@keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}

.container-loading {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #3232322b;
    z-index: 99;
    justify-content: center;
    align-items: center;
    display: flex;
}
</style>
@endsection


@section("content")

    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">TAMPILKAN DATA</label>
                      <select name="total_users" id="total_users" onchange="filterTotalUsers(this)" class="form-control">
                          <option value="10">10 Data</option>
                          <option value="15">15 Data</option>
                          <option value="30">30 Data</option>
                          <option value="all">Semua Data</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Keyword</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchingDescription(this);" placeholder="Masukan keyword (nama/deskripsi)">
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
          <div class="scroll-container" style="position: relative;">
              <div class="container-loading" id="container-loading">
                  <div class="">
                      <div class="mb-4 fw-bold text-primary">
                          <span>LOADING...</span>
                      </div>
                      <div class="" style="margin-left: 15px;transform: scale(1.5);">
                          <div class="hollowLoader">
                            <div class="largeBox">
                              <div class="smallBox"></div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          <div class="" id="container-users"></div>
      
@endsection

@section("js")
<script>
    var $loading = $("#container-loading").hide();
    let search = null;
    let totalUser = 10;
    const getLogUsers = async (endpoint) => {
           $loading.show();
           await fetch(endpoint+`?totalUser=${totalUser}&search=${search ?? ''}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-users").html("");
                    try {
                      let template = ``;
                      responseData.forEach(val => {
                          let label;
                          if(val.type_log == "join") {
                            label = "success";
                          } else if(val.type_log == "create") {
                            label = "primary";
                          } else if(val.type_log == "leave" || val.type_log == "kick") {
                            label = "danger";
                          } else if(val.type_log == "update-status") {
                            label = "info";
                          } else {
                            label = "warning";
                          }
                            template += `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1">
                                                            <p class="py-2 m-0 text-center w-100 rounded bg-label-${label}  me-1 fw-bold text-break">${val.description}</p>
                                                        </div>
                                                    </div>
                                            </div>`
                        });
                        $("#container-users").append(template)
                        
                    } catch (error) {
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#container-users").append(templateErr)
                    }

          });
        setTimeout(() => {
          $loading.hide();  
        }, 500);
  }

  getLogUsers('{{route('channel.user.logs', $code_channel)}}');

  const filterTotalUsers = (self) => {
      totalUser = $(self).val();
      getLogUsers('{{route('channel.user.logs', $code_channel)}}');
  }

  const searchingDescription = (self) => {
      search = $(self).val();
      getLogUsers('{{route('channel.user.logs', $code_channel)}}');
  }
</script>

@endsection
