@extends('template.master-with-sidebar')

@section("title")
 Daftar Kegiatan
@endsection

@section("css")
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<style>
    .font-07 {
        font-size: 0.7rem;
    }

    #container-events {
        /* display: flex; */
        overflow-x: hidden
    }

    .users {
    margin-bottom: 3px;
    border-radius: 10px;
    flex: 0 0 100%;
 }

 .scroll-container {
     height: 600px;
     overflow-y: scroll
 }
 
 .hollowLoader {
  width: 3em;
  height: 3em;
  -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
          animation: loaderAnim 1.25s infinite ease-in-out;
  outline: 1px solid transparent;
}
.hollowLoader .largeBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe52;
  outline: 1px solid transparent;
}
.hollowLoader .smallBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe;
  z-index: 1;
  outline: 1px solid transparent;
  -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
          animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
}

@-webkit-keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}

@keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}
@-webkit-keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}
@keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}

.container-loading {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #3232322b;
    z-index: 99;
    justify-content: center;
    align-items: center;
    display: flex;
}

.btnAddEvent {
  position: fixed;
    z-index: 999;
    bottom: 4rem;
    right: 2rem;
}

.btnAddEvent button {
  padding: 1rem 1rem;
    border-radius: 2.5rem;
    border: none;
    /* box-shadow: 0 0 7px 4px #a1a2ce; */
}

.swal2-container {
  z-index: 9999999999!important;
}

</style>
@endsection


@section("content")
    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">TAMPILKAN DATA</label>
                      <select name="total_users" id="total_users" onchange="filterTotalData(this)" class="form-control">
                          <option value="10">10 Data</option>
                          <option value="15">15 Data</option>
                          <option value="20">30 Data</option>
                          <option value="30">Semua Data</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Dari Tanggal</label>
                    <input type="text" id="datepicker" class="my-2" id="date" value="{{date("d-m-Y")}}" onchange="filterDate(this);" placeholder="">
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Nama Kegiatan</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchEventName(this);" placeholder="cari nama kegiatan...">
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
          <div class="scroll-container" style="position: relative;">
              <div class="container-loading" id="container-loading">
                  <div class="">
                      <div class="mb-4 fw-bold text-primary">
                          <span>LOADING...</span>
                      </div>
                      <div class="" style="margin-left: 15px;transform: scale(1.5);">
                          <div class="hollowLoader">
                            <div class="largeBox">
                              <div class="smallBox"></div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
           <div class="" id="container-events"></div>
@endsection

@section("js")
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js" integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
   var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
   $('#datepicker').datepicker({
            maxDate: today,
            format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap5'
    });
    var $loading = $("#container-loading").hide();
    let search = null;
    let status = "all";
    let date = '{{date("d-m-Y")}}';
    let totalData = 10;
    const getAbsentees = async (endpoint) => {
           $loading.show();
           await fetch(endpoint+`?total_data=${totalData}&status=${status}&date=${date}&search=${search ?? ''}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-events").html("");
                    try {
                        let template = ``;
                        let actionBtn  = ``;
                        responseData.forEach(val => {
                            template += `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold">Absensi Kegiatan :</div>
                                                        <span class="mb-1 text-primary fw-bold">${val.name_activity}</span>
                                                        <div class="mb-1 fw-bold">Tanggal :</div>
                                                        <span class="mb-1 text-primary fw-bold">${moment(val.created_at).format("MM-DD-YYYY H:mm")}</span>
                                                    </div>
                                            </div></div>`
                        });
                        $("#container-events").append(template)
                        
                    } catch (error) {
                        console.log(error);
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#container-events").append(templateErr)
                    }
          });
        setTimeout(() => {
          $loading.hide();  
        }, 500);
  }

  getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');

  const filterTotalData = (self) => {
      totalUser = $(self).val();
      getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
  }

  const filterDate = (self) => {
    date = $(self).val();
    getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
  }

  const changeStatus = (self) => {
    status = $(self).val();
    getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
  }

  const searchEventName = (self) => {
      search = $(self).val();
      getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
  }

  const openAbsentee = (self) => {
    let url = "{{route('channel.absentee.open',$code_channel)}}";
    let datastring = $("#formAbsentee").serialize();
    $.ajax({
          url: url,
          dataType: "json",
          type: "Post",
          async: true,
          data: datastring,
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              let absenteeId = $("select[name='list_absentee']").val();
              $(`option[value='${absenteeId}']`).remove();
              let listAbsenteeOptions = $("select[name='list_absentee'] option");
              console.log(listAbsenteeOptions.length);
              getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
              $("#exampleModal").modal("hide");
              if(listAbsenteeOptions.length == 0 ) {
                $("#listAbsentees").html(`<p class="font-italic bg-danger p-1 rounded fw-bold text-white text-center" style="font-style:italic">ABSENSI TIDAK TERSEDIA</p>`)
                $(self).remove();
              }
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const openModalEdit = (eventId, eventName, startAt, endAt) => {
      $("#formEditEvent input[name='eventId']").val(eventId);
      $("#formEditEvent input[name='name']").val(eventName);
      $("#formEditEvent input[name='startAt']").val(startAt);
      $("#formEditEvent input[name='endAt']").val(endAt);
      $("#exampleModal2").modal("show");
  }

  const closeAbsentee = (eventId, eventName) => {
       Swal.fire({
        title: 'Pesan!',
        text: `Apakah Anda ingin menutup absensi kegiatan ${eventName}?`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: "Tidak",
    }).then(val => {
      if(val.isConfirmed) {
        let url = "{{route('channel.absentee.close', $code_channel)}}"
        $.ajax({
          url: url,
          dataType: "json",
          type: "PUT",
          async: true,
          data: {eventId},
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              getAbsentees('{{route('channel.my-absentee.list', $code_channel)}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
        }); 
      }
    });
  }

  const printListAbsentees = (url, indexName) => {
        let print = window.open(url, indexName);
        setTimeout(() => {
            print.print();
            print.close();
        }, 200);
    }
</script>

@endsection
