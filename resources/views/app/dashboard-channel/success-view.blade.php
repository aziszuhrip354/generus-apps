<!DOCTYPE html>

<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="../assets/" data-template="vertical-menu-template-free">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Berhasil Absensi</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />


    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}"
        class="template-customizer-theme-css" />

    <style>
        form {
            width: 50%;
        }

        .bouncing-image {
            width: 100px;
            animation: bounce 2s ease-in-out infinite;
            /* Animasi naik turun */
        }

        /* Keyframes untuk animasi naik turun */
        @keyframes bounce {

            0%,
            100% {
                transform: translateY(0);
                /* Posisi awal dan akhir */
            }

            50% {
                transform: translateY(-20px);
                /* Posisi naik */
            }
        }

        /* Animasi terbang */
        @keyframes fly {
            0% {
                transform: translate(0, 0) rotate(0deg);
            }

            25% {
                transform: translate(300px, -50px) rotate(10deg);
            }

            50% {
                transform: translate(600px, 50px) rotate(-10deg);
            }

            75% {
                transform: translate(900px, -100px) rotate(15deg);
            }

            100% {
                transform: translate(1200px, 0) rotate(0deg);
            }
        }

        @media (max-width: 500px) {
            form {
                width: 100%;
            }

            #illustration {
                width: 60%!important;
            }
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row" style="min-height: 100vh">
            <div class="col-md-12 d-flex justify-content-center align-items-center">
                <form class="card rounded-2 border border-2 p-4">
                    <div class="text-center">
                        <img class="bouncing-image" id="illustration" style="width: 40%"
                            src="{{ $img ?? '' }}" alt="">
                    </div>
                    <h4 class="text-success bg-label-secondary p-2 px-3 rounded-2 fw-bold mt-2 text-center">{{$message ?? "Success"}}</h4>
            </div>
        </div>
    </div>
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('/js/bootstrap5.js') }}"></script>
    @yield('js')

    <script>
        setTimeout(() => {
            window.location.href = '{{route('user.dashboard')}}';
        }, 5000);
    </script>
    
</body>

</html>
