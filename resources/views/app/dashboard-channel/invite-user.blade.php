@extends('template.master-with-sidebar')

@section("title")
 Undang User    
@endsection

@section("css")
<style>
    .font-07 {
        font-size: 0.7rem;
    }

    #container-list-invitations {
        display: flex;
        overflow-x: hidden
    }

    .users {
    margin-bottom: 3px;
    border-radius: 10px;
    flex: 0 0 100%;
 }

 .c-pointer {
   cursor: pointer!important;
 }

 .t-inherit {
   text-transform: inherit!important;
 }

 .bg-transparansi {
  background-color: #ffffffa3 !important;
    box-shadow: -1px 11px 29px -5px #67676794;
 }

 .scroll-container {
     height: 600px;
     overflow-y: scroll
 }
 
 .hollowLoader {
  width: 3em;
  height: 3em;
  -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
          animation: loaderAnim 1.25s infinite ease-in-out;
  outline: 1px solid transparent;
}
.hollowLoader .largeBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe52;
  outline: 1px solid transparent;
}
.hollowLoader .smallBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe;
  z-index: 1;
  outline: 1px solid transparent;
  -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
          animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
}

.p-relative {
  position: relative;
}

.p-absolute {
  position: absolute;
  width: 100%!important;
  z-index: 100;
}

@-webkit-keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}

@keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}
@-webkit-keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}
@keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}

.mxh-400 {
  max-height: 400px;
}

.container-loading {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #3232322b;
    z-index: 99;
    justify-content: center;
    align-items: center;
    display: flex;
}

.swal2-container {
  z-index: 9999999999!important;
}

.scroll-y {
  overflow-y: scroll;
}
</style>
@endsection


@section("content")
<!-- Modal -->
<div class="modal fade" id="modalInvitation" tabindex="-1" aria-labelledby="modalInvitationLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalInvitationLabel">Form Pengundangan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="mb-3">
            <input type="hidden" id="user_id">
            <label for="username" class="form-label">Nama</label>
            <input type="text" class="form-control" disabled id="username" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="type_invitation" class="form-label">Metode Pengiriman Undangan</label>
            <select name="type_inviation" id="type_inviation" class="form-control">
              <option value="email">Email</option>
               {{-- <option value="wa">WA</option> --}}
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" onclick="sendInvitation();">Kirim</button>
      </div>
    </div>
  </div>
</div>

    <!--/ Basic Bootstrap Table -->
    <div class="card mb-4">
      <div class="col-md-12">
        <form class="mx-3 mt-3">
          <div class="row">
              <div class="col-md-12">
                  <div class="mb-3 p-relative">
                    <label for="exampleInputEmail1" class="form-label">Cari Nama User untuk Di Undang</label>
                    <input type="text" id="searchingName" onkeyup="searchUsernameInvititation(this);" class="form-control w-100" placeholder="cari nama user...">
                       <div class="bg-transparansi mxh-400 p-2 scroll-y p-absolute" style="margin-top: -1px" id="searchingUser" style="display: none">
                        
                       </div>
                  </div >
              </div>
          </div>
      </form>
      </div>
    </div>
    <div class="card mt-2">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">TAMPILKAN ANGGOTA</label>
                      <select name="total_users" id="total_users" onchange="filterTotaData(this)" class="form-control">
                          <option value="10">10 Data</option>
                          <option value="15">15 Data</option>
                          <option value="30">30 Data</option>
                          <option value="all">Semua Data</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Nama User</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchingName(this);" placeholder="cari nama anggota...">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Status Undangan</label>
                        <select name="total_users" id="total_users" onchange="filterStatus(this)" class="form-control">
                            <option value="all">Semua</option>
                            <option value="0">Tertunda</option>
                            <option value="1">Diterima</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
          <div class="scroll-container" style="position: relative;">
              <div class="container-loading" id="container-loading">
                  <div class="">
                      <div class="mb-4 fw-bold text-primary">
                          <span>LOADING...</span>
                      </div>
                      <div class="" style="margin-left: 15px;transform: scale(1.5);">
                          <div class="hollowLoader">
                            <div class="largeBox">
                              <div class="smallBox"></div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          <div class="" id="container-list-invitations"></div>
@endsection

@section("js")
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js" integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    moment.locale('id');  
    var $loading2 = $("#container-loading").hide();
    let search = null;
    let totalUser = 10;
    let gender = "all";
    const searchingUser = async (endpoint) => {
           $loading.show();
           await fetch(`${endpoint}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#searchingUser").html("");
                    try {
                        let template = ``;
                        responseData.forEach(val => {
                          template += `<div class="users c-pointer" onclick="openModalInvitation('${val.id}','${val.name}')">
                                        <div class="mb-2 box-shadow">
                                          <div class="card shadow p-3 py-2 mx-2">
                                            <table>
                                              <tr class="fw-bold">
                                                <td class="mb-2" style="width: 60px">Nama</td>
                                                <td>: <span class="badge t-inherit bg-label-success me-1 fw-bold">${val.name}</span></td>
                                              </tr>
                                              <tr class="fw-bold">
                                                <td class="mb-2">Email</td>
                                                <td>: <span class="badge t-inherit bg-label-info me-1 fw-bold">${val.email}</span></td>
                                              </tr>
                                              <tr class="fw-bold">
                                                <td class="mb-2">No WA</td>
                                                <td>: <span class="badge t-inherit bg-label-primary me-1 fw-bold">${val.no_hp}</span></td>
                                              </tr>
                                            </table>
                                          </div>
                                      </div>
                                    </div>`
                        });
                        $("#searchingUser").append(template)
                        $("#searchingUser").show(500);
                        
                    } catch (error) {
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#searchingUser").append(templateErr)
                    }

          });
        setTimeout(() => {
          $loading.hide();  
        }, 500);
  }

  let searchName = null;
  let totalData = 10;
  let status = "all";
  const listInvitations = async (endpoint) => {
           await fetch(`${endpoint}?total_data=${totalData}&status=${status}&search=${searchName}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-list-invitations").html("");
                    try {
                        let template = ``;
                        responseData.forEach(val => {
                          let label, text;
                          if(val.status == 1) {
                            label = "success";
                            message = "diterima";
                          } else if(val.status == 0) {
                             label = "warning";
                             message = "tertunda";
                          } else {
                            label = "danger";
                             message = "dibatalkan";
                          }
                          sentAt = moment(val.created_at).format("LLL");
                          template += `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold">Undangan Untuk :</div>
                                                        <span class="mb-1">${val.name}</span>
                                                        <div class="mb-1 fw-bold">Tanggal Undagan :</div>
                                                        <div class="mb-1">
                                                            <span class="badge bg-label-primary me-1 fw-bold">${sentAt}</span>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Undagan via :</div>
                                                        <div class="mb-1">
                                                            <span class="badge bg-label-${val.type_invitation == 'email' ? 'primary' : 'info' } me-1 fw-bold">${val.type_invitation.toUpperCase()}</span>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Diundang oleh :</div>
                                                        <div class="mb-1">
                                                            <span class="badge bg-label-warning me-1 fw-bold">${val.invited_by.toUpperCase()}</span>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Status :</div>
                                                        <div class="mb-1">
                                                            <span class="badge bg-label-${label} me-1 fw-bold">${message}</span>
                                                        </div>
                                                        ${val.status == 0 ? `<div class="my-2">
                                                            <button class="btn w-100 btn-danger" onclick="cancelInvitation('${val.random_code}','${val.name}')">Batalkan Undangan</button>
                                                        </div>` : ""}
                                                    </div>
                                            </div>`
                        });
                        $("#container-list-invitations").append(template)
                        $("#container-list-invitations").show(500);
                        
                    } catch (error) {
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#container-list-invitations").append(templateErr)
                    }
          });
  }
  listInvitations('{{route('channel.invitations', $code_channel)}}');

  const filterTotaData = (self) => {
      totalData = $(self).val();
      listInvitations('{{route('channel.invitations', $code_channel)}}');
  }

  const filterStatus = (self) => {
      status = $(self).val();
      listInvitations('{{route('channel.invitations', $code_channel)}}');
  }

  const searchingName = (self) => {
    searchName = $(self).val();
      listInvitations('{{route('channel.invitations', $code_channel)}}');
  }

  const searchUsernameInvititation = (self) => {
      search = $(self).val();
      if(search == '') {
        setTimeout(() => {
          $("#searchingUser").hide(500);
        }, 3000);
      } else {
        searchingUser(`{{route('channel.user.searching', [$code_channel,""])}}/${search}`);
      }
  }

  const openModalInvitation = (user_id, username) => {
    $("#modalInvitation").modal("show");
    $("#user_id").val(user_id);
    $("#username").val(username);
  }

  const sendInvitation = () => {
    let userId = $("#user_id").val();
    let type_inviation = $("#type_inviation").val();
    let url = "{{route('channel.invite.user', $code_channel)}}";
    $.ajax({
          url: url,
          dataType: "json",
          type: "Post",
          async: true,
          data: {userId, type_inviation},
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              $("#modalInvitation").modal("hide");
              listInvitations('{{route('channel.invitations', $code_channel)}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 5000
              });
          }
      }); 
      $("#searchingName").val("");
      $("#searchingUser").hide();
  }

  const cancelInvitation = (random_code, username) => {
    let url = "{{route('channel.invitations.cancel',$code_channel)}}";
    Swal.fire({
        title: 'Pesan!',
        text: `Apakah Anda ingin membatalkan undangan channel untuk ${username}?`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: "Tidak",
    }).then(val => {
      if(val.isConfirmed) {
        $.ajax({
          url: url,
          dataType: "json",
          type: "Post",
          async: true,
          data: {random_code},
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              listInvitations('{{route('channel.invitations', $code_channel)}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
      }); 
      }
    });
  }
  
</script>

@endsection
