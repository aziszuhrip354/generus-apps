<!DOCTYPE html>

<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-dark"
    data-assets-path="{{ url('/') }}/assets/" data-template="vertical-menu-template-free">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title> List Absen | {{ $findAbsentee->name_activity }}</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ url('/') }}/assets/img/favicon/favicon.ico" />


    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}"
        class="template-customizer-theme-css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .container-head {
            width: 220px;
            margin: 0 auto;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0.5rem 5.2rem;
            border: 2px solid #696cff;
        }

        .container-head-leave {
            border: 2px solid #ffab00 !important;
        }

        @media (max-width: 450px) {
            .d-flex {
                flex-direction: column;
            }
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="my-2">
                    <p class=" m-1 text-center">Absensi Kegiatan :</p>
                    <p class="fw-bold  m-1 text-center">{{ $findAbsentee->name_activity }}</p>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Tanggal Kegiatan :</p>
                    <p class="fw-bold  m-1 text-center">{{ date('d-M-Y', strtotime($findAbsentee->created_at)) }}</p>
                </div>
                <div class="d-flex justify-content-center mb-4 text-sm fw-bold">
                    <div class="mx-3">
                        <p class=" m-1 text-center">QR Code Presensi :</p>
                        <div class="container-head card">
                            <div id="qrcode"></div>
                        </div>
                    </div>
                    <div class="mx-3">
                        <p class=" m-1 text-center">QR Code Perizinan :</p>
                        <div class="container-head container-head-leave card">
                            <div id="qrcode_leave"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 d-flex justify-content-center">
                    <div id="pie-chart" style="width: 100%; height: 300px;"></div>
                </div>
                <div class="row d-flex justify-content-center text-center my-2 text-white">
                    <div class="col">
                        @if ($findAbsentee->status == 0)
                            <button class="btn btn-primary"
                                onclick="printListAbsentees('{{ route('channel.absentee.users.print', [$code_channel, $findAbsentee->code_absen]) }}','list-absentees.html')">
                                <i class="menu-icon p-0 m-0 tf-icons bx bx-printer"></i>
                            </button>
                        @endif
                        @if (user()->id == $findAbsentee->user_id || session('role_channel') == 'admin')
                            <button class="btn btn-success text-white"
                                onclick="printListAbsentees('{{ route('channel.absentee.users.print.qrcode', [$code_channel, $findAbsentee->code_absen]) }}','list-absentees.html')">
                                <i class="menu-icon p-0 m-0 tf-icons bx bx-code"></i>
                            </button>
                            <button class="btn btn-danger" onclick="closeAbsentee();">
                                <i class="menu-icon p-0 m-0 tf-icons bx bx-window-close"></i>
                            </button>
                        @endif
                    </div>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Dibuka Oleh :</p>
                    <p class="fw-bold  m-1 text-center">{{ $findAbsentee->name }}</p>
                </div>
                @php
                    $totalHadir = count(collect($findUserAbsentees)->where('type', 'present')->all());
                    $totalIzin = count(collect($findUserAbsentees)->where('type', 'leave')->all());
                @endphp
                <div class="my-2">
                    <p class=" m-1 text-center">Total Kehadiran :</p>
                    <p class="fw-bold  m-1 text-center" id="totalAbsentee">
                        {{ $totalHadir }} Orang</p>
                </div>
                <div class="my-2">
                    <p class=" m-1 text-center">Total Yang Izin :</p>
                    <p class="fw-bold  m-1 text-center" id="totalAbsenteeLeave">
                        {{ $totalIzin }} Orang</p>
                </div>
                <div class="my-2 d-flex justify-content-center">
                    <button type="button" class="btn btn-sm btn-primary fw-bold align-items-center"
                        data-bs-toggle="modal" data-bs-target="#modalAbsentManual">
                        <svg viewBox="0 0 24 24" width="20" height="20" stroke="currentColor" stroke-width="2"
                            fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                            <circle cx="12" cy="12" r="10"></circle>
                            <line x1="12" y1="8" x2="12" y2="16"></line>
                            <line x1="8" y1="12" x2="16" y2="12"></line>
                        </svg>
                        Absen Manual
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-md-6" id="man">
            <p class="text-center fw-bold text-uppercase my-4">Daftar Hadir Laki-LAKI</p>
            <table class="w-100 my-3 table table-striped rounded" id="tableMan">
                <tr class="rounded shadow">
                    <td class="text-center fw-bold text-primary">#</td>
                    <td class="text-center fw-bold text-primary">NAMA</td>
                    <td class="text-center fw-bold text-primary">JAM HADIR</td>
                </tr>
                @php
                    $counterMan = 1;
                    $counterWomen = 1;
                    $lastIdAbsent = 0;
                @endphp
                @foreach ($findUserAbsentees as $absent)
                    @if ($absent->gender == 'pria' && $absent->type == 'present')
                        <tr class="">
                            <td class="text-center">{{ $counterMan++ }}</td>
                            <td class="text-left text-uppercase fw-semibold text-primary">{{ $absent->name }}
                                <div class="fw-bold text-danger" style="font-size: 11px;"><a
                                        onclick="deleteAbsent({{ $absent->id }})" class="cursor-pointer">Hapus</a>
                                </div>
                            </td>
                            <td class="text-center">{{ $absent->created_at ? date('d-m-Y H:i', strtotime($absent->created_at)) : '[ABSENT_MANUAL]' }}</td>
                        </tr>
                    @endif
                    @php
                        $lastIdAbsent = $absent->id;
                    @endphp
                @endforeach
            </table>
        </div>
        <div class="col-md-6" id="women">
            <p class="text-center fw-bold text-uppercase my-4">Daftar Hadir PEREMPUAN</p>
            <table class="w-100 my-3 table table-striped" id="tableWomen">
                <tr class="rounded shadow">
                    <td class="text-center fw-bold text-primary">#</td>
                    <td class="text-center fw-bold text-primary">NAMA</td>
                    <td class="text-center fw-bold text-primary">JAM HADIR</td>
                </tr>
                @foreach ($findUserAbsentees as $absent)
                    @if ($absent->gender == 'wanita' && $absent->type == 'present')
                        <tr>
                            <td class="text-center">{{ $counterWomen++ }}</td>
                            <td class="text-left text-uppercase fw-semibold text-primary">{{ $absent->name }}
                                <div class="fw-bold text-danger" style="font-size: 11px;"><a
                                        onclick="deleteAbsent({{ $absent->id }})" class="cursor-pointer">Hapus</a>
                                </div>
                            </td>
                            <td class="text-center">{{ $absent->created_at ? date('d-m-Y H:i', strtotime($absent->created_at)) : '[ABSENT_MANUAL]' }}</td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
        <div class="col-md-6" id="man">
            <p class="text-center fw-bold text-uppercase my-4">Daftar Izin Laki-LAKI</p>
            <table class="w-100 my-3 table table-striped" id="tableManLeave">
                <tr class="rounded shadow">
                    <td class="text-center fw-bold text-primary">#</td>
                    <td class="text-center fw-bold text-primary">NAMA</td>
                    <td class="text-center fw-bold text-primary">ALASAN</td>
                </tr>
                @php
                    $counterMan = 1;
                    $counterWomen = 1;
                    $lastIdAbsent = 0;
                @endphp
                @foreach ($findUserAbsentees as $absent)
                    @if ($absent->gender == 'pria' && $absent->type == 'leave')
                        <tr>
                            <td class="text-center">{{ $counterMan++ }}</td>
                            <td class="texleft text-uppercase">{{ $absent->name }} <div><span style="font-size: 9px"
                                        class="text-sm border text-uppercase fw-bold badge badge-xs bg-label-secondary text-primary">{{ str_replace('_', ' ', $absent->leave_category) }}</span>
                                </div>
                            </td>
                            <td class="text-left text-xs">{{ $absent->reason }}</td>
                        </tr>
                    @endif
                    @php
                        $lastIdAbsent = $absent->id;
                    @endphp
                @endforeach
            </table>
        </div>
        <div class="col-md-6" id="women">
            <p class="text-center fw-bold text-uppercase my-4">Daftar Izin PEREMPUAN</p>
            <table class="w-100 my-3 table table-striped" id="tableWomen Leave">
                <tr class="rounded shadow">
                    <td class="text-center fw-bold text-primary">#</td>
                    <td class="text-center fw-bold text-primary">NAMA</td>
                    <td class="text-center fw-bold text-primary">ALASAN</td>
                </tr>
                @foreach ($findUserAbsentees as $absent)
                    @if ($absent->gender == 'wanita' && $absent->type == 'leave')
                        <tr>
                            <td class="text-center">{{ $counterWomen++ }}</td>
                            <td class="text-left text-uppercase">{{ $absent->name }}
                                <div><span style="font-size: 9px"
                                        class="text-sm border text-uppercase fw-bold badge badge-xs bg-label-secondary text-primary">{{ str_replace('_', ' ', $absent->leave_category) }}</span>
                                </div>
                            </td>
                            <td class="text-left text-xs">{{ $absent->reason }}</td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalAbsentManual" tabindex="-1" aria-labelledby="modalAbsentManualLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fw-bold text-primary" id="modalAbsentManualLabel">Absen
                        Manual</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formAbsentManual" onsubmit="submitAbsentManual(event, this)">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label fw-bold text-primary">Tipe</label>
                            <select name="tipe" onchange="selectTypeManualAbsent(this);" id="tipe"
                                class="form-control text-uppercase">
                                <option value="present">Kehadiran</option>
                                <option value="leave">Perizinan</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label fw-bold text-primary">List
                                Pengguna</label>
                            <div class="w-100">
                                <select name="user[]" id="user" multiple="multiple"
                                    class="form-control text-uppercase select2" style="width: 100%!important">
                                    @foreach ($userUnaction as $uac)
                                        <option value="{{ $uac->id }}">{{ $uac->user->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 formLeave" style="display: none">
                            <label for="leave_category" class="form-label fw-bold text-primary">Kategori Udzur</label>
                            <select name="leave_category" class="form-control text-uppercase" id="leave_category">
                                <option value="Sakit">Sakit</option>
                                <option value="diluar_kota">Diluar Kota</option>
                                <option value="Kerja">Kerja</option>
                                <option value="kegiatan_kuliah">Kegiatan Kuliah</option>
                                <option value="lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="mb-3 formLeave" style="display: none">
                            <label for="reason" class="form-label fw-bold text-primary">Keterangan
                                Izin (Pesan)</label>
                            <textarea name="reason" id="reason" class="form-control" cols="10" rows="5"
                                placeholder="Masukan alasan perizinan...">{{ old('reason') }}</textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" id="buttonSubmit" class="btn btn-primary"
                        onclick="buttonSubmit(this);">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('/js/qrcode.js') }}"></script>
    <script src="{{ asset('/js/swal.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('/assets/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js"
        integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $.ajaxSetup({
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        })
        $(".select2").select2({
            dropdownParent: $("#modalAbsentManual"),
            containerCssClass: 'p-4',
        });
        const openModal = () => {
            $("#modalAbsentManual").modal('show');
        }

        const selectTypeManualAbsent = (self) => {
            let val = $(self).val();
            if (val == 'leave') {
                $('.formLeave').slideDown(200);
            } else {
                $('.formLeave').slideUp(200);
            }
        }

        $(document).ready(function() {
            Highcharts.setOptions({
                colors: ['#696cff', '#ffab00', '#ff3e1d', '#4370ad', '#ad4343'], // Set warna tema
                chart: {
                    backgroundColor: '#f5f5f9', // Latar belakang chart
                },
                title: {
                    style: {
                        color: '#333', // Warna judul
                        fontSize: '24px'
                    }
                },
                tooltip: {
                    style: {
                        backgroundColor: '#fffff',
                        color: '#333'
                    }
                }
            });
            let data = [];
            data.push({
                name: 'Hadir',
                y: {{ $totalHadir }},
                total: {{ $totalHadir }},
            });
            data.push({
                name: 'Izin',
                y: {{ $totalIzin }},
                total: {{ $totalIzin }},
            });
            data.push({
                name: 'Tanpa Keterangan',
                y: {{ $totalUsers - $totalHadir - $totalIzin }},
                total: {{ $totalUsers - $totalHadir - $totalIzin }},
            })

            // Menghitung total keseluruhan
            const grandTotal = data.reduce((sum, category) => sum + category.total, 0);

            // // Menambahkan field persentase ke setiap kategori
            data.forEach(category => {
                let percentage = ((category.y / grandTotal) * 100); // Menghitung persentase
                category.percent = percentage.toFixed(2);
            });

            // Membuat Pie Chart
            Highcharts.chart('pie-chart', {
                title: '',
                chart: {
                    type: 'pie',
                    backgroundColor: null,
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: 'gray', // Ubah warna teks data label di sini
                                fontWeight: 'bold',
                                textOutline: 'none' // Hilangkan outline pada teks
                            }
                        }
                    }
                },
                series: [{
                    name: 'Persentase',
                    colorByPoint: true,
                    data: data
                }],
                tooltip: {
                    pointFormat: '<b>Total {point.name}: {point.y} ({point.percent}% dari ' +
                        ')</b>'
                }
            });
        });
        let counterMan = parseInt('{{ $counterMan }}');
        let counterWomen = parseInt('{{ $counterWomen }}');
        let lastIdAbsent = parseInt('{{ $lastIdAbsent }}');
        let totalAbsentee = parseInt('{{ count(collect($findUserAbsentees)->where('type', 'present')->all()) }}')
        let totalAbsenteeLeave = parseInt('{{ count(collect($findUserAbsentees)->where('type', 'leave')->all()) }}')
        const getDataAbsentee = async (endpoint) => {
            await fetch(endpoint + `?lastIdAbsent=${lastIdAbsent}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    try {
                        let templateMan = ``;
                        let templateWomen = ``;
                        let templateManLeave = ``;
                        let templateWomenLeave = ``;
                        responseData.forEach(val => {
                            if (val.gender == "pria" && val.type == 'present') {
                                templateMan += `<tr>
                                                    <td class="text-center">${counterMan++}</td>
                                                    <td class="text-center">${val.name}
                                                    <div class="fw-bold text-danger" style="font-size: 11px;"><button onclick="deleteAbsent(${val.id})"
                                                        class="cursor-pointer">Hapus</button></div>
                                                        </td>
                                                    <td class="text-center">${moment(val.created_at).format("MM-DD-YYYY H:mm")}</td>
                                                </tr>`;
                                totalAbsentee++;
                            } else if (val.gender == "wanita" && val.type == 'present') {
                                templateWomen += `<tr>
                                                    <td class="text-center">${counterWomen++}</td>
                                                    <td class="text-center">${val.name}
                                                        <div class="fw-bold text-danger" style="font-size: 11px;"><button onclick="deleteAbsent(${val.id})"
                                                        class="cursor-pointer">Hapus</button></div>
                                                    </td>
                                                    <td class="text-center">${moment(val.created_at).format("MM-DD-YYYY H:mm")}</td>
                                                </tr>`;
                                totalAbsentee++;
                            } else if (val.gender == "pria" && val.type == 'leave') {
                                templateManLeave += `<tr>
                                                    <td class="text-center">${counterWomen++}</td>
                                                    <td class="text-center">${val.name}</td>
                                                    <td class="text-left text-xs">${val.reason}</td>
                                                </tr>`;
                                totalAbsenteeLeave++;
                            } else {
                                templateWomenLeave += `<tr>
                                                    <td class="text-center">${counterWomen++}</td>
                                                    <td class="text-center">${val.name}</td>
                                                    <td class="text-left text-xs">${val.reason}</td>
                                                </tr>`
                                totalAbsenteeLeave++;
                            }
                            lastIdAbsent = val.id;
                        });
                        $("#totalAbsentee").text(`${totalAbsentee} Orang`);
                        $("#totalAbsenteeLeave").text(`${totalAbsenteeLeave} Orang`);
                        $("#tableMan").append(templateMan)
                        $("#tableWomen").append(templateWomen)
                        $("#tableManLeave").append(templateManLeave)
                        $("#tableWomenLeave").append(templateWomenLeave)

                    } catch (error) {

                    }
                });
        }

        @if ($findAbsentee->status == 1)
            setInterval(() => {
                getDataAbsentee("{{ route('channel.absentee.users.list', [$code_channel, $findAbsentee->id]) }}")
            }, 3000);
        @endif

        var qr = document.getElementById("qrcode");
        var qrcode = new QRCode(qr, {
            text: "{{ route('channel.absentee.users', [$code_channel, $findAbsentee->code_absen]) }}",
            width: 200,
            height: 200,
            colorDark: "#696cff",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

        var qrLeave = document.getElementById("qrcode_leave");
        var qrcodeLeave = new QRCode(qrLeave, {
            text: "{{ route('channel.absentee.users-leave', [$code_channel, $findAbsentee->code_absen]) }}",
            width: 200,
            height: 200,
            colorDark: "#ffab00",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

        @if (session('errors'))
            Swal.fire({
                title: 'Pesan!',
                text: "{{ session('errors') }}",
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            });
        @endif
        @if (session('success'))
            Swal.fire({
                title: 'Pesan!',
                text: "{{ session('success') }}",
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 3000
            });
        @endif

        const printListAbsentees = (url, indexName) => {
            let print = window.open(url, indexName);
            setTimeout(() => {
                print.print();
                print.close();
            }, 200);
        }

        const deleteAbsent = (id) => {
            Swal.fire({
                title: 'Pesan!',
                text: `Apakah Anda ingin menghapus data absen ini?`,
                icon: 'warning',
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: "Tidak",
            }).then(val => {
                if (val.isConfirmed) {
                    let url = "{{ route('channel.absentee.delete-absent', [$code_channel, '/']) }}/" + id
                    $.ajax({
                        url: url,
                        dataType: "json",
                        type: "DELETE",
                        async: true,
                        success: function(data) {
                            Swal.fire({
                                title: 'Pesan!',
                                text: data.message,
                                icon: 'success',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        },
                        error: function(xhr, exception) {
                            let error = xhr.responseJSON;
                            Swal.fire({
                                title: 'Pesan!',
                                text: error.message,
                                icon: 'error',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });
        }

        const closeAbsentee = () => {
            let eventId = "{{ base64_encode($findAbsentee->id) }}";
            Swal.fire({
                title: 'Pesan!',
                text: `Apakah Anda ingin menutup absensi kegiatan '{{ $findAbsentee->name_activity }}'?`,
                icon: 'warning',
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: "Tidak",
            }).then(val => {
                if (val.isConfirmed) {
                    let url = "{{ route('channel.absentee.close', $code_channel) }}"
                    $.ajax({
                        url: url,
                        dataType: "json",
                        type: "PUT",
                        async: true,
                        data: {
                            eventId
                        },
                        success: function(data) {
                            Swal.fire({
                                title: 'Pesan!',
                                text: data.message,
                                icon: 'success',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        },
                        error: function(xhr, exception) {
                            let error = xhr.responseJSON;
                            Swal.fire({
                                title: 'Pesan!',
                                text: error.message,
                                icon: 'error',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }
            });
        }

        const submitAbsentManual = (e, self) => {
            e.preventDefault();
            let data = $(self).serialize();
            let url = '{{ route('channel.absentee.manual-absent', [$code_channel, $findAbsentee->id]) }}';
            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                async: true,
                data: data,
                beforeSend: function() {
                    $("#buttonSubmit").attr('disabled', true);
                    $("#buttonSubmit").html(`<div class="spinner-border text-primary spinner-border-sm" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>`);
                },
                success: function(data) {
                    Swal.fire({
                        title: 'Pesan!',
                        text: data.message,
                        icon: 'success',
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 2000);
                },
                error: function(xhr, exception) {
                    $("#buttonSubmit").attr('disabled', false);
                    $("#buttonSubmit").html('Submit');
                    $("#modalAbsentManual").hide();
                    let error = xhr.responseJSON;
                    Swal.fire({
                        title: 'Pesan!',
                        text: error.message,
                        icon: 'error',
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 2000
                    });

                    setTimeout(() => {
                        $("#modalAbsentManual").show();
                    }, 2500);
                }
            });
        }

        const buttonSubmit = () => {
            $("#formAbsentManual").submit();
        }
    </script>
    @yield('js')
</body>

</html>
