@extends('template.master-with-sidebar')

@section('title')
    {{ $findChannel->channel_name }}
@endsection

@section('css')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <div class="d-flex align-items-end row">
                    <div class="col-sm-7">
                        <div class="card-body">

                            <h5 class="card-title text-primary">Hallo
                                {{ user()->gender == 'pria' ? 'Mas, ' : 'Mba, ' }}{{ user()->name }}! 🎉</h5>
                            <p class="mb-4">
                                Selamat Datang Di {{ $findChannel->channel_name }}
                            </p>

                            {{-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">Analisis</a> --}}
                        </div>
                    </div>
                    <div class="col-sm-5 text-center text-sm-left">
                        <div class="card-body pb-0 px-0 px-md-4">
                            <img src="../assets/img/illustrations/man-with-laptop-light.png" height="140"
                                alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                data-app-light-img="illustrations/man-with-laptop-light.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <input type="text" id="clipboard_input" class="d-none">
                <div class="" id="container-channel">
                </div>
            </div>
        </div>
        <!--/ Total Revenue -->
        <div class="col-sm-12 col-md-5 col-lg-5 order-3 order-md-2">
            <div class="row">
                <div class="col-md-6 col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title mb-4">
                                <div class="avatar flex-shrink-0">
                                    <button class="btn btn-primary d-flex justify-content-center">
                                        <svg viewBox="0 0 24 24" width="30" height="30" stroke="currentColor"
                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                            class="css-i6dzq1">
                                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="9" cy="7" r="4"></circle>
                                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <h5 class="fw-semibold d-block mb-1">Total Jamaah</h5>
                            <h2 class="card-title fw-bold mb-2 text-primary">{{ $totalMember }}</h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title mb-4">
                                <div class="avatar flex-shrink-0">
                                    <button class="btn btn-info d-flex justify-content-center">
                                        <svg viewBox="0 0 24 24" width="30" height="30" stroke="currentColor"
                                            stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                            class="css-i6dzq1">
                                            <path
                                                d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z">
                                            </path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <h5 class="fw-semibold d-block mb-1">Total Kegiatan</h5>
                            <h2 class="card-title fw-bold mb-2 text-info">{{ $totalEvent }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-7 col-lg-7 order-3 order-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h2 class="fw-semibold d-block mb-1 text-center">Komposisi Jamaah</h2>
                    </div>
                    <div id="pie-chart" style="width: 100%; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    {{-- <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
        aria-labelledby="staticBackdropLabel" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fw-bold" id="staticBackdropLabel">TAMBAH CHANNEL</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="container-opsi-channel d-flex">
                            <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary bdr-primary fw-bold"
                                id="create-btn" onclick="changeOptionChannel(this);">BARU</a>
                            <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary" id="join-btn"
                                onclick="changeOptionChannel(this);">GABUNG</a>
                        </div>
                        <div class="my-2">
                            <form class="">
                                <div class="mb-3" id="input-new-channel">
                                    <label for="exampleInputPassword1" class="form-label">Nama Channel</label>
                                    <input type="text" class="form-control" onkeyup="toUpperCase(this);"
                                        id="exampleInputPassword1">
                                </div>
                                <div class="mb-3" id="input-code-channel" style="display: none">
                                    <label class="form-label" for="phoneNumber">CODE CHANNEL</label>
                                    <div class="input-group input-group-merge">
                                        <span class="input-group-text fw-bold">#</span>
                                        <input type="text" id="phoneNumber" name="phoneNumber" class="form-control"
                                            placeholder="XXXXXX">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-add-channel" data-button="create"
                        onclick="tambahChannel(this)">Buat</button>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            Highcharts.setOptions({
                colors: ['#2e3073', '#6cd130', '#ad4371', '#4370ad', '#ad4343'], // Set warna tema
                chart: {
                    backgroundColor: '#f4f4f4', // Latar belakang chart
                },
                title: {
                    style: {
                        color: '#333', // Warna judul
                        fontSize: '24px'
                    }
                },
                tooltip: {
                    style: {
                        backgroundColor: '#fffff',
                        color: '#333'
                    }
                }
            });
            let data = [];
            @foreach ($dataCharts as $key => $chart)
                data.push({
                    name: '{{ $key }}',
                    y: parseInt('{{ $chart }}'),
                    total: parseInt('{{ $chart }}'),
                })
            @endforeach

            // Menghitung total keseluruhan
            const grandTotal = data.reduce((sum, category) => sum + category.total, 0);

            // Menambahkan field persentase ke setiap kategori
            data.forEach(category => {
                let percentage = ((category.y / grandTotal) * 100); // Menghitung persentase
                category.percent = percentage.toFixed(2);
            });

            // Membuat Pie Chart
            Highcharts.chart('pie-chart', {
                title: '',
                chart: {
                    type: 'pie',
                    backgroundColor: '#ffffff'
                },
                series: [{
                    name: 'Persentase',
                    colorByPoint: true,
                    data: data
                }],
                tooltip: {
                    pointFormat: '<b>Total {point.name}: {point.y} ({point.percent}% dari ' +
                        grandTotal +
                        ')</b>'
                }
            });
        });
    </script>
@endsection
