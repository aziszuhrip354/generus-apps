@extends('template.master-with-sidebar')

@section('title')
    Daftar User
@endsection

@section('css')
    <style>
        .font-07 {
            font-size: 0.7rem;
        }

        #container-users {
            display: flex;
            overflow-x: hidden
        }

        .users {
            margin-bottom: 3px;
            border-radius: 10px;
            flex: 0 0 100%;
        }

        .scroll-container {
            height: 600px;
            overflow-y: scroll
        }

        .hollowLoader {
            width: 3em;
            height: 3em;
            -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
            animation: loaderAnim 1.25s infinite ease-in-out;
            outline: 1px solid transparent;
        }

        .hollowLoader .largeBox {
            height: 3em;
            width: 3em;
            background-color: #6a6cfe52;
            outline: 1px solid transparent;
        }

        .hollowLoader .smallBox {
            height: 3em;
            width: 3em;
            background-color: #6a6cfe;
            z-index: 1;
            outline: 1px solid transparent;
            -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
            animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
        }

        @-webkit-keyframes smallBoxAnim {
            0% {
                transform: scale(0.2);
            }

            100% {
                transform: scale(0.75);
            }
        }

        @keyframes smallBoxAnim {
            0% {
                transform: scale(0.2);
            }

            100% {
                transform: scale(0.75);
            }
        }

        @-webkit-keyframes loaderAnim {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(90deg);
            }
        }

        @keyframes loaderAnim {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(90deg);
            }
        }

        .container-loading {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #3232322b;
            z-index: 99;
            justify-content: center;
            align-items: center;
            display: flex;
        }
    </style>
@endsection


@section('content')
    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">TAMPILKAN ANGGOTA</label>
                        <select name="total_users" id="total_users" onchange="filterTotalUsers(this)" class="form-control text-uppercase">
                            <option value="10">10 Anggota</option>
                            <option value="15">15 Anggota</option>
                            <option value="30">30 Anggota</option>
                            <option value="all">Semua Anggota</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                  <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Nama User</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchUsername(this);"
                          placeholder="cari nama anggota...">
                  </div>
              </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Jenis Kelamin</label>
                        <select name="total_users" id="total_users" onchange="filterGender(this)" class="form-control text-uppercase">
                            <option value="all">Semua</option>
                            <option value="pria">Pria</option>
                            <option value="wanita">Wanita</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Kategori</label>
                        <select name="total_users" id="total_users" onchange="filterCategory(this)" class="form-control text-uppercase">
                            <option value="all">Semua</option>
                            @foreach ($roles as $role)
                                <option value="{{$role->name}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <div class="scroll-container" style="position: relative;">
            <div class="container-loading" id="container-loading">
                <div class="">
                    <div class="mb-4 fw-bold text-primary">
                        <span>LOADING...</span>
                    </div>
                    <div class="" style="margin-left: 15px;transform: scale(1.5);">
                        <div class="hollowLoader">
                            <div class="largeBox">
                                <div class="smallBox"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="" id="container-users"></div>
        @endsection

        @section('js')
            <script>
                var $loading = $("#container-loading").hide();
                let search = null;
                let totalUser = 10;
                let gender = "all";
                let category = "all";
                const getUsers = async (endpoint) => {
                    $loading.show();
                    await fetch(endpoint + `?totalUser=${totalUser}&gender=${gender}&category=${category}&search=${search ?? ''}`)
                        .then((response) => response.json())
                        .then((data) => {
                            let responseData = data.data;
                            $("#container-users").html("");
                            try {
                                let actionButton = '';
                                let template = `<div class="users row d-flex justify-content-center">`;
                                responseData.forEach(val => {
                                    actionButton = '';
                                    if ('{{ $findUserChannel->type_user }}' == 1 && val.user_id !=
                                        '{{ user()->id }}') {
                                        actionButton = `<div class="d-flex justify-content-end my-2">
                                                    ${'{{ user()->id }}' != val.user_id ? `<div class="mx-2">
                                                                    <button onclick="changeStatusUser('${val.user_id}','${val.type_user}','${val.name}');" class="btn font-07 btn-primary">
                                                                      <svg viewBox="0 0 24 24" width="20" height="20" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="1" y="5" width="22" height="14" rx="7" ry="7"></rect><circle cx="8" cy="12" r="3"></circle></svg>
                                                                    </button>
                                                                </div>` : ''}
                                                    <div class="mx-1">
                                                        <button class="btn font-07 btn-danger" onclick="kickUser('${val.user_id}','${val.name}');">
                                                          <svg viewBox="0 0 24 24" width="20" height="20" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                                        </button>
                                                    </div>
                                                </div>`;
                                    }
                                    template += `<div class="mx-2 my-4 box-shadow col-md-5">
                                                    <div class="card shadow p-3 py-2 row">
                                                        <div class="row">
                                                          <div class="col-md-8">
                                                              <div class="mb-1 fw-bold">Nama :</div>
                                                              <span class="mb-1">${val.name}</span>
                                                              <div class="mb-1 fw-bold">Jenis Kelamin :</div>
                                                              <div class="mb-1">
                                                                  <span class="badge bg-label-${val.gender == 'pria' ? 'danger' : 'info' } me-1 fw-bold">${val.gender.toUpperCase()}</span>
                                                              </div>
                                                              <div class="mb-1 fw-bold">Status :</div>
                                                              <div class="mb-1">
                                                                  <span class="badge bg-label-${val.type_user == 1 ? 'success' : 'warning' } me-1 fw-bold">${val.type_user == 1 ? 'ADMIN' : 'ANGGOTA' }</span>
                                                                  <span class="badge bg-label-secondary me-1 fw-bold">${val.role }</span>
                                                              </div>
                                                              <div class="mb-1 fw-bold">Nomor WA :</div>
                                                              <div class="mb-1">
                                                                  <span class="badge bg-label-primary me-1 fw-bold">${val.no_hp}</span>
                                                              </div>
                                                          </div>
                                                          <div class="col-md-4 my-2 d-flex align-items-center justify-content-center">
                                                            <div class="p-2 shadow rounded-2">
                                                                <img style='width:100%;' class="my-2" src='{{ asset('assets/img/avatars') }}/${val.gender == "pria" ? 'man' : 'woman'}-profile.svg'>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        ${actionButton}            
                                                    </div>
                                                 </div>
                                            `;
                                });
                                template += '</div>'
                                console.log(template);

                                $("#container-users").append(template)

                            } catch (error) {
                                let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                                $("#container-users").append(templateErr)
                            }

                        });
                    setTimeout(() => {
                        $loading.hide();
                    }, 500);
                }

                getUsers('{{ route('channel.user.list.all', $code_channel) }}');

                const filterTotalUsers = (self) => {
                    totalUser = $(self).val();
                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                }

                const filterGender = (self) => {
                    gender = $(self).val();
                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                }

                const filterCategory = (self) => {
                    category = $(self).val();
                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                }

                const searchUsername = (self) => {
                    search = $(self).val();
                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                }

                const changeStatusUser = (user_id, type_user, username) => {
                    let message;
                    let url = '{{ route('channel.change-status.user', $code_channel) }}'
                    if (type_user == 1) {
                        message = `Apakah Anda ingin menguah status ${username} menjadi anggota biasa`;
                    } else {
                        message = `Apakah Anda ingin menguah status ${username} menjadi admin`;
                    }

                    Swal.fire({
                        title: 'Pesan!',
                        text: message,
                        icon: 'warning',
                        showCancelButton: true,
                        showConfirmButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: "Tidak",
                    }).then(val => {
                        if (val.isConfirmed) {
                            $.ajax({
                                url: url,
                                dataType: "json",
                                type: "Post",
                                async: true,
                                data: {
                                    user_id
                                },
                                success: function(data) {
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: data.message,
                                        icon: 'success',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                                },
                                error: function(xhr, exception) {
                                    let error = xhr.responseJSON;
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: error.message,
                                        icon: 'error',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                }
                            });
                        }
                    });
                }

                const kickUser = (user_id, username) => {
                    let url = "{{ route('channel.kick.user', $code_channel) }}"
                    Swal.fire({
                        title: 'Pesan!',
                        text: `Apakah Anda ingin mengeluarkan ${username} dari channel ini?`,
                        icon: 'warning',
                        showCancelButton: true,
                        showConfirmButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: "Tidak",
                    }).then(val => {
                        if (val.isConfirmed) {
                            $.ajax({
                                url: url,
                                dataType: "json",
                                type: "Post",
                                async: true,
                                data: {
                                    user_id,
                                    username
                                },
                                success: function(data) {
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: data.message,
                                        icon: 'success',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                    getUsers('{{ route('channel.user.list.all', $code_channel) }}');
                                },
                                error: function(xhr, exception) {
                                    let error = xhr.responseJSON;
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: error.message,
                                        icon: 'error',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                }
                            });
                        }
                    });
                }
            </script>
        @endsection
