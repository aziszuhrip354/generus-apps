@extends('template.master-with-sidebar')

@section('title')
    Daftar Kegiatan
@endsection

@section('css')
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"> --}}

    <style>
        .font-07 {
            font-size: 0.7rem;
        }

        #container-events {
            /* display: flex; */
            overflow-x: hidden
        }

        .users {
            margin-bottom: 3px;
            border-radius: 10px;
            /* flex: 0 0 100%; */
        }

        .scroll-container {
            height: 600px;
            overflow-y: scroll
        }

        .hollowLoader {
            width: 3em;
            height: 3em;
            -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
            animation: loaderAnim 1.25s infinite ease-in-out;
            outline: 1px solid transparent;
        }

        .hollowLoader .largeBox {
            height: 3em;
            width: 3em;
            background-color: #6a6cfe52;
            outline: 1px solid transparent;
        }

        .hollowLoader .smallBox {
            height: 3em;
            width: 3em;
            background-color: #6a6cfe;
            z-index: 1;
            outline: 1px solid transparent;
            -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
            animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
        }

        @-webkit-keyframes smallBoxAnim {
            0% {
                transform: scale(0.2);
            }

            100% {
                transform: scale(0.75);
            }
        }

        @keyframes smallBoxAnim {
            0% {
                transform: scale(0.2);
            }

            100% {
                transform: scale(0.75);
            }
        }

        @-webkit-keyframes loaderAnim {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(90deg);
            }
        }

        @keyframes loaderAnim {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(90deg);
            }
        }

        .container-loading {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #3232322b;
            z-index: 99;
            justify-content: center;
            align-items: center;
            display: flex;
        }

        .btnAddEvent {
            position: fixed;
            z-index: 999;
            bottom: 4rem;
            right: 2rem;
        }

        .btnAddEvent button {
            padding: 1rem 1rem;
            border-radius: 2.5rem;
            border: none;
            /* box-shadow: 0 0 7px 4px #a1a2ce; */
        }

        .swal2-container {
            z-index: 9999999999 !important;
        }

        .input-group-append {
            display: flex;
        }
    </style>
@endsection


@section('content')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Buat Absensi</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formAbsentee">
                        <div class="mb-3" id="listAbsentees">
                            @if (count($absenteeValid))
                                <label for="absentee_id" class="form-label">Daftar Absensi</label>
                                <select class="form-control" name="absentee_id" id="">
                                    @foreach ($absenteeValid as $absentee)
                                        <option value="{{ base64_encode($absentee->id) }}"
                                            id="{{ base64_encode($absentee->id) }}">{{ $absentee->name_activity }}</option>
                                    @endforeach
                                </select>
                            @else
                                <p class="font-italic bg-danger p-1 rounded fw-bold text-white text-center"
                                    style="font-style:italic">ABSENSI TIDAK TERSEDIA</p>
                            @endif
                        </div>
                        @if (count($absenteeValid))
                            <div class="mb-3">
                                <label for="role" class="form-label">Berikan kepada</label>
                                <select class="select2" name="assign_role" id="role" multiple="multiple"
                                    style="width: 100%">
                                    @foreach ($listRole as $role)
                                        <option value="{{ $role }}" class="text-uppercase">{{ $role }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </form>
                </div>
                <div class="modal-footer">
                    @if (count($absenteeValid))
                        <button type="button" class="btn btn-primary" onclick="openAbsentee(this);">Buat</button>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="btnAddEvent">
        <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none"
                stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                <circle cx="12" cy="12" r="10"></circle>
                <line x1="12" y1="8" x2="12" y2="16"></line>
                <line x1="8" y1="12" x2="16" y2="12"></line>
            </svg>
        </button>
    </div>
    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">TAMPILKAN DATA</label>
                        <select name="total_users" id="total_users" onchange="filterTotalData(this)" class="form-control">
                            <option value="10">10 Data</option>
                            <option value="15">15 Data</option>
                            <option value="30">30 Data</option>
                            <option value="all">Semua Data</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Dari Tanggal</label>
                        <input type="text" class="form-control" id="start_date" onchange="filterDate(this);"
                            placeholder="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Sampai Tanggal</label>
                        <input type="text" class="form-control" id="end_date" onchange="filterDate(this);"
                            placeholder="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Cari Nama Kegiatan</label>
                        <input type="text" class="form-control" id="searching_user" onkeyup="searchEventName(this);"
                            placeholder="cari nama kegiatan...">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Status Absensi</label>
                        <select name="status" class="form-control" id="" onchange="changeStatus(this);">
                            <option value="all">SEMUA</option>
                            <option value="1">AKTIF</option>
                            <option value="0">DITUTUP</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <div class="scroll-container" style="position: relative;">
            <div class="container-loading" id="container-loading">
                <div class="">
                    <div class="mb-4 fw-bold text-primary">
                        <span>LOADING...</span>
                    </div>
                    <div class="" style="margin-left: 15px;transform: scale(1.5);">
                        <div class="hollowLoader">
                            <div class="largeBox">
                                <div class="smallBox"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-wrap flex-row" id="container-events"></div>
        @endsection

        @section('js')
            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment-with-locales.min.js"
                integrity="sha512-vFABRuf5oGUaztndx4KoAEUVQnOvAIFs59y4tO0DILGWhQiFnFHiR+ZJfxLDyJlXgeut9Z07Svuvm+1Jv89w5g=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>

            <script>
                moment.locale("id_ID");
                $(document).ready(function() {
                    $('#start_date').datepicker({
                        uiLibrary: 'bootstrap4',
                        format: "dd-mm-yyyy",
                        theme: 'bootstrap',
                        maxDate: moment().format('DD-MM-YYYY'),
                        value: moment().startOf('month').format('DD-MM-YYYY'),
                    });
                    $('#end_date').datepicker({
                        uiLibrary: 'bootstrap4',
                        format: "dd-mm-yyyy",
                        theme: 'bootstrap',
                        value: moment().format('DD-MM-YYYY'),
                        maxDate: moment().format('DD-MM-YYYY'),
                    });
                });


                $(".select2").select2({
                    dropdownParent: $("#exampleModal"),
                });

                var $loading = $("#container-loading").hide();
                let search = null;
                let status = "all";
                let start_date = moment().startOf('month').format('DD-MM-YYYY');
                let end_date = moment().format('DD-MM-YYYY');
                let totalData = 10;
                const getAbsentees = async (endpoint) => {
                    $loading.show();
                    await fetch(endpoint +
                            `?total_data=${totalData}&status=${status}&start_date=${start_date}&end_date=${end_date}&search=${search ?? ''}`
                            )
                        .then((response) => response.json())
                        .then((data) => {
                            let responseData = data.data;
                            $("#container-events").html("");
                            try {
                                let template = ``;
                                let actionBtn = ``;
                                responseData.forEach(val => {
                                    if ('{{ session('role_channel') }}' == 'admin' && val.status == 1) {
                                        actionBtn = `<div class="mx-1">
                                              <button class="btn w-100 btn-danger" onclick="closeAbsentee('${btoa(val.id)}','${val.name_activity}')">
                                                <i class="menu-icon p-0 m-0 tf-icons bx bx-window-close"></i>
                                              </button>
                                            </div>`

                                    }

                                    template += `<div class="users w-100">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold">Absensi Kegiatan :</div>
                                                        <div>
                                                            <span class="mb-1 text-primary fw-bold">${val.name_activity}</span>
                                                            <small class='badge badge-xs bg-label-primary ml-2 fw-bold text-xs'>${moment(val.created_at).format('dddd, D MMMM YYYY')}</small>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Dibuka Oleh :</div>
                                                        <div class='mb-1'>
                                                            <div class="badge badge-xs bg-label-secondary fw-bold">${val.name}</div>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Jadwal :</div>
                                                        <div class='mb-1'>
                                                            <span class="badge badge-xs bg-label-warning fw-bold">${val.start_at} s/d ${val.end_at}</span>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Total Kehadiran :</div>
                                                        <div class='mb-1'>
                                                            <div class="badge badge-xs bg-label-info fw-bold">${val.total_absen} Orang</div>
                                                        </div>
                                                        <div class="mb-1 fw-bold">Status :</div>
                                                        <div class='mb-1'>
                                                          <span class="badge bg-label-${val.status == 1 ? "success" : "danger"} fw-bold">${val.status == 1 ? "AKTIF" : "TELAH DITUTUP"}</span>
                                                        </div
                                                        <div>
                                                          <div class="d-flex justify-content-end mt-2">
                                                            <div class="mx-1">
                                                              <a target="_blank" href="{{ route('channel.absentee.users.index', [$code_channel, '']) }}/${val.code_absen}" class="btn w-100 btn-success">
                                                                <i class="menu-icon p-0 m-0 tf-icons bx bxs-user-detail"></i>
                                                              </a>
                                                            </div>
                                                            <div class="mx-1">
                                                            <button class="btn w-100 btn-primary" onclick="printListAbsentees('{{ route('channel.absentee.users.print', [$code_channel, '']) }}/${val.code_absen}','list-absentees.html')">
                                                                <i class="menu-icon p-0 m-0 tf-icons bx bx-printer"></i>
                                                            </button>
                                                            </div>
                                                            ${actionBtn}
                                                          </div>
                                                        </div>
                                                    </div>
                                            </div></div>`
                                });
                                $("#container-events").append(template)

                            } catch (error) {
                                console.log(error);
                                let templateErr = `<div class="users w-100">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                                $("#container-events").append(templateErr)
                            }

                        });
                    setTimeout(() => {
                        $loading.hide();
                    }, 500);
                }

                getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');

                const filterTotalData = (self) => {
                    totalData = $(self).val();
                    getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                }

                const filterDate = (self) => {
                    start_date = $("#start_date").val();
                    end_date = $("#end_date").val();
                    getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                }

                const changeStatus = (self) => {
                    status = $(self).val();
                    getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                }

                const searchEventName = (self) => {
                    search = $(self).val();
                    getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                }

                const openAbsentee = (self) => {
                    let url = "{{ route('channel.absentee.open', $code_channel) }}";
                    let absentee_id = $("select[name='absentee_id']").val();
                    let assign_role = $("select[name='assign_role']").val();

                    $.ajax({
                        url: url,
                        dataType: "json",
                        type: "Post",
                        async: true,
                        data: {
                            absentee_id,
                            assign_role
                        },
                        success: function(data) {
                            Swal.fire({
                                title: 'Pesan!',
                                text: data.message,
                                icon: 'success',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                            let absenteeId = $("select[name='list_absentee']").val();
                            $(`option[value='${absenteeId}']`).remove();
                            let listAbsenteeOptions = $("select[name='list_absentee'] option");
                            console.log(listAbsenteeOptions.length);
                            getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                            $("#exampleModal").modal("hide");
                            if (listAbsenteeOptions.length == 0) {
                                $("#listAbsentees").html(
                                    `<p class="font-italic bg-danger p-1 rounded fw-bold text-white text-center" style="font-style:italic">ABSENSI TIDAK TERSEDIA</p>`
                                )
                                $(self).remove();
                            }
                        },
                        error: function(xhr, exception) {
                            let error = xhr.responseJSON;
                            Swal.fire({
                                title: 'Pesan!',
                                text: error.message,
                                icon: 'error',
                                showCancelButton: false,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    });
                }

                const openModalEdit = (eventId, eventName, startAt, endAt) => {
                    $("#formEditEvent input[name='eventId']").val(eventId);
                    $("#formEditEvent input[name='name']").val(eventName);
                    $("#formEditEvent input[name='startAt']").val(startAt);
                    $("#formEditEvent input[name='endAt']").val(endAt);
                    $("#exampleModal2").modal("show");
                }

                const closeAbsentee = (eventId, eventName) => {
                    Swal.fire({
                        title: 'Pesan!',
                        text: `Apakah Anda ingin menutup absensi kegiatan ${eventName}?`,
                        icon: 'warning',
                        showCancelButton: true,
                        showConfirmButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: "Tidak",
                    }).then(val => {
                        if (val.isConfirmed) {
                            let url = "{{ route('channel.absentee.close', $code_channel) }}"
                            $.ajax({
                                url: url,
                                dataType: "json",
                                type: "PUT",
                                async: true,
                                data: {
                                    eventId
                                },
                                success: function(data) {
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: data.message,
                                        icon: 'success',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                    getAbsentees('{{ route('channel.absentee.list', $code_channel) }}');
                                },
                                error: function(xhr, exception) {
                                    let error = xhr.responseJSON;
                                    Swal.fire({
                                        title: 'Pesan!',
                                        text: error.message,
                                        icon: 'error',
                                        showCancelButton: false,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                }
                            });
                        }
                    });
                }

                const printListAbsentees = (url, indexName) => {
                    let print = window.open(url, indexName);
                    setTimeout(() => {
                        print.print();
                        print.close();
                    }, 200);
                }
            </script>
        @endsection
