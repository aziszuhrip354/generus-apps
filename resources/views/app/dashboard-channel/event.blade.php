@extends('template.master-with-sidebar')

@section("title")
 Daftar Kegiatan
@endsection

@section("css")
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<style>
    .font-07 {
        font-size: 0.7rem;
    }

    #container-events {
        display: flex;
        overflow-x: hidden
    }

    .users {
    margin-bottom: 3px;
    border-radius: 10px;
    flex: 0 0 100%;
 }

 .scroll-container {
     height: 600px;
     overflow-y: scroll
 }
 
 .hollowLoader {
  width: 3em;
  height: 3em;
  -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
          animation: loaderAnim 1.25s infinite ease-in-out;
  outline: 1px solid transparent;
}
.hollowLoader .largeBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe52;
  outline: 1px solid transparent;
}
.hollowLoader .smallBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe;
  z-index: 1;
  outline: 1px solid transparent;
  -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
          animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
}

@-webkit-keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}

@keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}
@-webkit-keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}
@keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}

.container-loading {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #3232322b;
    z-index: 99;
    justify-content: center;
    align-items: center;
    display: flex;
}

.btnAddEvent {
  position: fixed;
    z-index: 999;
    bottom: 4rem;
    right: 2rem;
}

.btnAddEvent button {
  padding: 1rem 1rem;
    border-radius: 2.5rem;
    border: none;
    /* box-shadow: 0 0 7px 4px #a1a2ce; */
}

.swal2-container {
  z-index: 9999999999!important;
}

</style>
@endsection


@section("content")

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Buat Kegiatan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="formEvent">
            <div class="mb-3">
              <label for="eventName" class="form-label">Durasi kegiatan</label>
              <input type="text" class="form-control" id="eventName" name="name" placeholder="masukan nama kegiatan..">
            </div>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label w-100 text-center mb-2 fw-bold py-1">Durasi Kegiatan</label>
              <div class="row">
                <div class="col">
                  <label for="exampleInputEmail1" class="form-label text-center w-100">Jam Mulai</label>
                  <input id="timepicker" class="form-control w-100" name="startAt" width="100%" />
                </div>
                <div class="col">
                  <label for="exampleInputEmail1" class="form-label text-center w-100">Jam Berakhir</label>
                  <input id="timepicker2" class="form-control w-100" name="endAt" width="100%" />
                </div>
              </div>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="createNewEvent();">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModal2Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModal2Label">Buat Kegiatan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="formEditEvent">
            <input type="hidden" name="eventId">
            <div class="mb-3">
              <label for="eventName" class="form-label">Nama kegiatan</label>
              <input type="text" class="form-control" id="eventName" name="name" placeholder="masukan nama kegiatan..">
            </div>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label w-100 text-center mb-2 fw-bold py-1">Durasi Kegiatan</label>
              <div class="row">
                <div class="col">
                  <label for="exampleInputEmail1" class="form-label text-center w-100">Jam Mulai</label>
                  <input id="timepicker3" class="form-control w-100" name="startAt" width="100%" />
                </div>
                <div class="col">
                  <label for="exampleInputEmail1" class="form-label text-center w-100">Jam Berakhir</label>
                  <input id="timepicker4" class="form-control w-100" name="endAt" width="100%" />
                </div>
              </div>
            </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="editEvent();">Simpan</button>
        </div>
      </div>
    </div>
  </div>

    <div class="btnAddEvent">
      <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
      </button>
    </div>
    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">TAMPILKAN DATA</label>
                      <select name="total_users" id="total_users" onchange="filterTotalUsers(this)" class="form-control">
                          <option value="10">10 Data</option>
                          <option value="15">15 Data</option>
                          <option value="20">30 Data</option>
                          <option value="30">Semua Data</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Nama Kegiatan</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchEventName(this);" placeholder="cari nama kegiatan...">
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
          <div class="scroll-container" style="position: relative;">
              <div class="container-loading" id="container-loading">
                  <div class="">
                      <div class="mb-4 fw-bold text-primary">
                          <span>LOADING...</span>
                      </div>
                      <div class="" style="margin-left: 15px;transform: scale(1.5);">
                          <div class="hollowLoader">
                            <div class="largeBox">
                              <div class="smallBox"></div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          <div class="" id="container-events"></div>

@endsection

@section("js")
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
{{-- <script src="{{asset("js/qrcode.js")}}"></script> --}}
<script>
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
   $('#timepicker').timepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });
    $('#timepicker2').timepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });

    $('#timepicker3').timepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });
    $('#timepicker4').timepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });
    var $loading = $("#container-loading").hide();
    let search = null;
    let totalData = 10;
    const getEvents = async (endpoint) => {
           $loading.show();
           await fetch(endpoint+`?total_data=${totalData}&search=${search ?? ''}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-events").html("");
                    try {
                        let template = ``;
                        responseData.forEach(val => {
                            template += `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold">Nama Kegiatan :</div>
                                                        <span class="mb-1 text-primary fw-bold">${val.name_activity}</span>
                                                        <div class="mb-1 fw-bold">Jam :</div>
                                                        <span class="mb-1 text-warning fw-bold">${val.start_at} s/d ${val.end_at}</span>
                                                        <div class="row my-2">
                                                          <div class=" mx-3 btn btn-primary col" onclick="openModalEdit('${btoa(val.id)}','${val.name_activity}','${val.start_at}','${val.end_at}')">
                                                            <i class="menu-icon p-0 m-0 tf-icons bx bxs-edit"></i>
                                                          </div>
                                                          <div class=" mx-3 btn btn-danger col" onclick="deleteEvent('${btoa(val.id)}','${val.name_activity}')">
                                                            <i class="menu-icon p-0 m-0 tf-icons bx bxs-trash"></i>
                                                          </div>
                                                        </div>
                                                    </div>
                                            </div>`
                        });
                        $("#container-events").append(template)
                        
                    } catch (error) {
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#container-events").append(templateErr)
                    }

          });
        setTimeout(() => {
          $loading.hide();  
        }, 500);
  }

  getEvents('{{route('channel.event.list', $code_channel)}}');

  const filterTotalUsers = (self) => {
      totalUser = $(self).val();
      getEvents('{{route('channel.event.list', $code_channel)}}');
  }

  const searchEventName = (self) => {
      search = $(self).val();
      getEvents('{{route('channel.event.list', $code_channel)}}');
  }

  const createNewEvent = () => {
    let url = "{{route('channel.event.create',$code_channel)}}";
    let datastring = $("#formEvent").serialize();
    $.ajax({
          url: url,
          dataType: "json",
          type: "Post",
          async: true,
          data: datastring,
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              getEvents('{{route('channel.event.list', $code_channel)}}');
              $("#exampleModal").modal("hide");
              $("#formEvent input").val("");
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const editEvent = (self) => {
    let url = "{{route('channel.event.update',$code_channel)}}";
    let datastring = $("#formEditEvent").serialize();
    $.ajax({
          url: url,
          dataType: "json",
          type: "PUT",
          async: true,
          data: datastring,
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              getEvents('{{route('channel.event.list', $code_channel)}}');
              $("#exampleModal2").modal("hide");
              $("#formEditEvent input").val("");
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const openModalEdit = (eventId, eventName, startAt, endAt) => {
      $("#formEditEvent input[name='eventId']").val(eventId);
      $("#formEditEvent input[name='name']").val(eventName);
      $("#formEditEvent input[name='startAt']").val(startAt);
      $("#formEditEvent input[name='endAt']").val(endAt);
      $("#exampleModal2").modal("show");
  }

  const deleteEvent = (eventId, eventName) => {
       Swal.fire({
        title: 'Pesan!',
        text: `Apakah Anda ingin menghapus kegiatan ${eventName}?`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: "Tidak",
    }).then(val => {
      if(val.isConfirmed) {
        let url = "{{route('channel.event.delete', $code_channel)}}"
        $.ajax({
          url: url,
          dataType: "json",
          type: "DELETE",
          async: true,
          data: {eventId},
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              getEvents('{{route('channel.event.list', $code_channel)}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
        }); 
      }
    });
  }

</script>

@endsection
