@extends('template.master')

@section('title')
    Dashboard
@endsection

@section('css')
    <style>
        #container-channel {
            display: flex;
            overflow-y: hidden;
        }

        .channel {
            width: 350px;
            padding: 5px;
            margin-right: -20px;
            margin-bottom: 10px;
            border-radius: 10px;
            flex: 0 0 17rem;
        }

        /* .modal.show .modal-dialog {
        transform: translateY(0) scale(1.15);
    } */

        .w-50 {
            width: 50%
        }

        .bdr-primary {
            border-bottom: 2px solid #696cff;
            transition: 0.2s;
        }

        .swal2-container {
            z-index: 9999999999 !important;
        }

        .clipboard {
            padding: 0 0.2rem;
            display: flex;
        }

        .clipboard-input {
            border-top-left-radius: 0.3rem;
            border-bottom-left-radius: 0.3rem;
            border: none;
            background-color: #e9ecef;
            border: 1px solid #696cff;
            padding: 0.5rem;
            font-weight: bold;
            color: #696cff;
        }

        .clipboard-btn {
            border: none;
            padding: 0.5rem;
            border-top-right-radius: 0.3rem;
            border-bottom-right-radius: 0.3rem;
            border: 1px solid #696cff;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <div class="d-flex align-items-end row">
                    <div class="col-sm-7">
                        <div class="card-body">

                            <h5 class="card-title text-primary">Hallo
                                {{ user()->gender == 'pria' ? 'Mas, ' : 'Mba, ' }}{{ user()->name }}! 🎉</h5>
                            <p class="mb-4">
                                Selamat Datang Di Generus Apps
                            </p>

                            {{-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">Analisis</a> --}}
                        </div>
                    </div>
                    <div class="col-sm-5 text-center text-sm-left">
                        <div class="card-body pb-0 px-0 px-md-4">
                            <img src="../assets/img/illustrations/man-with-laptop-light.png" height="140"
                                alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                data-app-light-img="illustrations/man-with-laptop-light.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 mb-2 order-0">
            <nav class=" w-100 layout-navbar container-xxl d-flex rounded" id="layout-navbar">
                <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                    <!-- Search -->
                    <div class="navbar-nav align-items-center">
                        <div class="nav-item d-flex align-items-center">
                            <i class="bx bx-search fs-4 lh-0"></i>
                            <input type="text" class="form-control border-0 shadow-none" placeholder="Cari Channel..."
                                aria-label="Cari Channel...">
                        </div>
                    </div>
                    <!-- /Search -->

                    <ul class="navbar-nav flex-row align-items-center ms-auto">
                        <!-- Place this tag where you want the button to render. -->
                        <li class="nav-item lh-1 me-3">
                            <span>
                                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="#ffffff" stroke-width="2"
                                        fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg>
                                </button>
                            </span>
                        </li>

                        <!-- User -->
                        <li class="nav-item navbar-dropdown dropdown-user dropdown">

                        </li>
                        <!--/ User -->
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <input type="text" id="clipboard_input" class="d-none">
                <div class="" id="container-channel">
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
        aria-labelledby="staticBackdropLabel" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fw-bold" id="staticBackdropLabel">TAMBAH CHANNEL</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="">
                        <div class="container-opsi-channel d-flex">
                            <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary bdr-primary fw-bold"
                                id="create-btn" onclick="changeOptionChannel(this);">BARU</a>
                            <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary" id="join-btn"
                                onclick="changeOptionChannel(this);">GABUNG</a>
                        </div>
                        <div class="my-2">
                            <form class="">
                                <div class="mb-3" id="input-new-channel">
                                    <label for="exampleInputPassword1" class="form-label">Nama Channel</label>
                                    <input type="text" class="form-control" onkeyup="toUpperCase(this);"
                                        id="exampleInputPassword1">
                                </div>
                                <div class="mb-3" id="input-code-channel" style="display: none">
                                    <label class="form-label" for="phoneNumber">CODE CHANNEL</label>
                                    <div class="input-group input-group-merge">
                                        <span class="input-group-text fw-bold">#</span>
                                        <input type="text" id="phoneNumber" name="phoneNumber" class="form-control"
                                            placeholder="XXXXXX">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-add-channel" data-button="create"
                        onclick="tambahChannel(this)">Buat</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        const changeOptionChannel = (self) => {
            let idBtn = $(self).attr("id");
            if (idBtn == "join-btn") {
                $("#create-btn").removeClass("bdr-primary");
                $("#create-btn").removeClass("fw-bold");
                $(self).addClass("bdr-primary");
                $(self).addClass("fw-bold");
                $("#input-new-channel").slideUp(300);
                $("#input-new-channel").hide(300);
                $("#input-code-channel").slideDown(300);
                $("#btn-add-channel").text("Gabung");
                $("#btn-add-channel").attr("data-button", "join");
            } else {
                $("#join-btn").removeClass("bdr-primary");
                $("#join-btn").removeClass("fw-bold");
                $(self).addClass("bdr-primary");
                $(self).addClass("fw-bold");
                $("#input-code-channel").slideUp(300);
                $("#input-code-channel").hide(300);
                $("#input-new-channel").slideDown(500);
                $("#btn-add-channel").text("Buat");
                $("#btn-add-channel").attr("data-button", "create");
            }
        }

        const tambahChannel = (self) => {
            let dataButton = $(self).attr("data-button");
            let data, url;
            if (dataButton == "create") {
                data = $("#input-new-channel input").val();
                url = "{{ route('channel.create') }}";
            } else {
                data = $("#input-code-channel input").val();
                url = "{{ route('channel.join') }}";
            }

            $.ajax({
                url: url,
                dataType: "json",
                type: "Post",
                async: true,
                data: {
                    data
                },
                success: function(data) {
                    Swal.fire({
                        title: 'Pesan!',
                        text: data.message,
                        icon: 'success',
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $("#container-channel").hide(200);
                    let strRes = data.data.channel_name.substr(0, 15);
                    let template = `<div class="channel">
                            <div class="mx-2 my-4 box-shadow">
                                <div class="card shadow">
                                  <div class="card-body">
                                    <h5 class="card-title">${data.data.channel_name.length > 15 ? strRes+"..." : strRes}</h5>
                                    <div class="mb-1">
                                      <div class="clipboard">
                                        <input type="text" class="w-100 clipboard-input" value='#${data.data.code_channel}' id="channeling" disabled="">
                                        <button class="clipboard-btn fw-bold text-primary" id="clipboard-link" data-button='#${data.data.code_channel}' onclick="clipboard(this);">
                                          COPY
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                  <img class="img-fluid" src="../assets/img/icons/grup-icon.jpeg" alt="Card image cap">
                                  <div class="card-body">
                                      <a href="{{ route('channel.dashboard', '') }}/${data.data.code_channel}" class="btn btn-primary w-100">MASUK</a>
                                  </div>
                                </div>
                              </div>
                          </div>`
                    $("#container-channel").prepend(template)
                    $("#container-channel").slideDown(200);
                    $("#staticBackdrop").modal("hide");
                },
                error: function(xhr, exception) {
                    let error = xhr.responseJSON;
                    Swal.fire({
                        title: 'Pesan!',
                        text: error.message,
                        icon: 'error',
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    data = $("#input-new-channel input").val("");
                    data = $("#input-code-channel input").val("");
                }
            });
        }

        const toUpperCase = (self) => {
            let text = $(self).val();
            $(self).val(text.toUpperCase());
        }

        let offsetGetChannel = 0;
        let manyData = 6;
        const getChannels = async (endpoint) => {
            await fetch(endpoint + `?offset=${offsetGetChannel}&total_data=${manyData++}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-channel").html("");
                    responseData.forEach(val => {
                        let strRes = val.channel_name.substr(0, 15);
                        let template = `<div class="channel">
                                      <div class="mx-2 my-4 box-shadow">
                                          <div class="card shadow">
                                            <div class="card-body">
                                              <h5 class="card-title">${val.channel_name.length > 15 ? strRes+"..." : strRes}</h5>
                                              <div 1lass="mb-3">
                                                <div class="clipboard">
                                                  <input type="text" class="w-100 clipboard-input" value='#${val.code_channel}' id="channeling" disabled="">
                                                  <button class="clipboard-btn fw-bold text-primary" id="clipboard-link" data-button='#${val.code_channel}' onclick="clipboard(this);">
                                                    COPY
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                            <img class="img-fluid" src="../assets/img/icons/grup-icon.jpeg" alt="Card image cap">
                                            <div class="card-body">
                                                <a href="{{ route('channel.dashboard', '') }}/${val.code_channel}" class="btn btn-primary w-100">MASUK</a>
                                            </div>
                                          </div>
                                        </div>
                                    </div>`
                        $("#container-channel").append(template)
                    });
                });
        }
        const clipboard = (self) => {
            /* Get the text field */
            var copyText = document.getElementById("clipboard_input");
            let link = $("#clipboard_input").val($(self).attr("data-button"));
            console.log($(self).attr("data-button"));

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value.replace("#", ""));

            /* Alert the copied text */
            Swal.fire({
                icon: 'success',
                title: "Pesan!",
                text: "Berhasil salin kode channel: " + link.val(),
                showConfirmButton: false,
                timer: 2000
            });
        }
        getChannels("{{ route('channel.get.all') }}")
        @if (session('error'))
            Swal.fire({
                title: 'Pesan!',
                text: "{{ session('error') }}",
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            });
        @endif
    </script>
@endsection
