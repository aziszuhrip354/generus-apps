@extends('template.master')

@section("title")
QR Code Scanner
@endsection


@section("content")

    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
          <li class="nav-item"> 
            <a class="nav-link active text-center" href="{{route('user.dashboard')}}"><i class=""></i> KEMBALI KE DASHBOARD</a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" href="pages-account-settings-notifications.html"><i class="bx bx-bell me-1"></i> Notifications</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages-account-settings-connections.html"><i class="bx bx-link-alt me-1"></i> Connections</a>
          </li> --}}
        </ul>
        <div class="card mb-4">
          <h5 class="card-header fw-bold text-center">QR Code Scanner</h5>
          
          <hr class="my-0">
          <div class="card-body">
            <div class="qrcode" id="qrcode-scanner">
            </div>
          </div>
        </div>
    </div>
@endsection

@section("js")
<script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
<script>
 function onScanSuccess(decodedText, decodedResult) {
  // handle the scanned code as you like, for example:
  
  try {
    console.log(`Code matched = ${decodedText}`, decodedResult);
    window.location.href = decodedText;
  } catch (error) {
    console.log("QR CODE IS NOT VALID");
  }

}

function onScanFailure(error) {
  // handle scan failure, usually better to ignore and keep scanning.
  // for example:
  // console.warn(`Code scan error = ${error}`);
}

let html5QrcodeScanner = new Html5QrcodeScanner(
  "qrcode-scanner",
  { fps: 10, qrbox: {width: 250, height: 250} },
  /* verbose= */ false);
html5QrcodeScanner.render(onScanSuccess, onScanFailure);
</script>
@endsection
