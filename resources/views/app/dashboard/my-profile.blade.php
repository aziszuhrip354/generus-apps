@extends('template.master')

@section("title")
My Profile
@endsection


@section("content")

    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-md-row mb-3">
          <li class="nav-item"> 
            <a class="nav-link active text-center" href="{{route('user.dashboard')}}"><i class=""></i> KEMBALI KE DASHBOARD</a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" href="pages-account-settings-notifications.html"><i class="bx bx-bell me-1"></i> Notifications</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages-account-settings-connections.html"><i class="bx bx-link-alt me-1"></i> Connections</a>
          </li> --}}
        </ul>
        <div class="card mb-4">
          <h5 class="card-header">Profile Details</h5>
          <!-- Account -->
          <div class="card-body">
            <div class="d-flex align-items-start align-items-sm-center gap-4">
              <img src="../assets/img/avatars/1.png" alt="user-avatar" class="d-block rounded" height="100" width="100" id="uploadedAvatar">
              <div class="button-wrapper">
                <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                  <span class="d-none d-sm-block">Upload new photo</span>
                  <i class="bx bx-upload d-block d-sm-none"></i>
                  <input type="file" id="upload" class="account-file-input" hidden="" accept="image/png, image/jpeg">
                </label>
                <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                  <i class="bx bx-reset d-block d-sm-none"></i>
                  <span class="d-none d-sm-block">Reset</span>
                </button>

                <p class="text-muted mb-0">Allowed JPG, GIF or PNG. Max size of 800K</p>
              </div>
            </div>
          </div>
          <hr class="my-0">
          <div class="card-body">
            <form id="formAccountSettings" method="POST" onsubmit="return false">
              <div class="row">
                <div class="mb-3 col-md-6">
                  <label for="name" class="form-label">Nama<span class="text-danger">*</span> </label>
                  <input class="form-control" type="text" id="name" name="name" value="{{user()->name}}" autofocus="">
                </div>
                <div class="mb-3 col-md-6">
                    <label for="email" class="form-label">E-mail<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" id="email" name="email" value="{{user()->email}}" placeholder="john.doe@example.com">
                </div>
                <div class="mb-3 col-md-6">
                    <label for="gender" class="form-label">Jenis Kelamin<span class="text-danger">*</span></label>
                    <select id="gender" name="gender" class="select2 form-select">
                      <option {{user()->gender == "pria" ? "selected" : ""}} value="pria">PRIA</option>
                      <option {{user()->gender == "wanita" ? "selected" : ""}} value="wanita">WANITA</option>
                    </select>
                </div>
                <div class="mb-3 col-md-6">
                  <label for="asal_daerah" class="form-label">ASAL DAERAH</label>
                  <input type="text" class="form-control" id="asal_daerah" value="{{user()->asal_daerah}}" name="asal_daerah">
                </div>
                <div class="mb-3 col-md-6">
                  <label class="form-label" for="no_hp">NOMOR WA<span class="text-danger">*</span></label>
                  <div class="input-group input-group-merge">
                    <span class="input-group-text">+62</span>
                    <input type="text" id="no_hp" name="no_hp" value="{{$wa}}" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="mb-3 col-md-6">
                  <label for="address" class="form-label">ALAMAT TEMPAT TINGGAL SAAT INI</label>
                  <input type="text" class="form-control" id="address" name="address" value="{{user()->alamat}}" placeholder="">
                </div>
                <div class="mb-3 col-md-6">
                  <label for="kampus" class="form-label">KAMPUS</label>
                  <input class="form-control" type="text" value="{{$getCollege ? $getCollege->nama_kampus : ""}}" id="kampus" name="kampus" placeholder="">
                </div>
                <div class="mb-3 col-md-6">
                  <label for="prodi" class="form-label">JURUSAN KAMPUS</label>
                  <input type="text" value="{{$getCollege ? $getCollege->jurusan : ""}}" class="form-control" id="prodi" name="prodi" placeholder="" maxlength="50">
                </div>
              </div>
              <div class="mt-2">
                <button type="submit" class="btn btn-primary me-2" onclick="sendDataAccount();">Simpan</button>
                <button type="reset" class="btn btn-outline-secondary">Batal</button>
              </div>
            </form>
          </div>
          <!-- /Account -->
        </div>
        {{-- <div class="card">
          <h5 class="card-header">Delete Account</h5>
          <div class="card-body">
            <div class="mb-3 col-12 mb-0">
              <div class="alert alert-warning">
                <h6 class="alert-heading fw-bold mb-1">Are you sure you want to delete your account?</h6>
                <p class="mb-0">Once you delete your account, there is no going back. Please be certain.</p>
              </div>
            </div>
            <form id="formAccountDeactivation" onsubmit="return false">
              <div class="form-check mb-3">
                <input class="form-check-input" type="checkbox" name="accountActivation" id="accountActivation">
                <label class="form-check-label" for="accountActivation">I confirm my account deactivation</label>
              </div>
              <button type="submit" class="btn btn-danger deactivate-account">Deactivate Account</button>
            </form>
          </div>
        </div>
      </div> --}}
    </div>
@endsection

@section("js")
<script>
    const sendDataAccount = () => {
        let datastring = $("#formAccountSettings").serialize();
        let url = "{{route('user.my-profile.post')}}"
        $.ajax({
        url: url,
        dataType: "json",
        type: "Post",
        async: true,
        data: datastring,
        success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            });
        },
        error: function (xhr, exception) {
            let error = xhr.responseJSON;
            Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            });
        }
    }); 
    }
</script>
@endsection
